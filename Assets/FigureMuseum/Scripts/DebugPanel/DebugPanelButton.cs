﻿using DG.Tweening;
using UnityEngine;

public class DebugPanelButton : MonoBehaviour 
{
	[SerializeField]
	private DebugPanel _debugPanel;
	private int _activationClickCount = 10;
	private int _clickCount = 0;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	private void TimeOut()
	{
		DOTween.Kill("TimeOutDelay." + GetInstanceID());
		_clickCount = 0;
	}





	public void OnClick()
	{
		if(_clickCount == 0)
		{
			DOTween.Kill("TimeOutDelay." + GetInstanceID());
			DOVirtual.DelayedCall(3f, TimeOut).SetId("TimeOutDelay." + GetInstanceID());
		}

		_clickCount++;

		if(_clickCount >= _activationClickCount)
		{
			DOTween.Kill("TimeOutDelay." + GetInstanceID());
			_clickCount = 0;

			_debugPanel.Show();
		}
	}
}
