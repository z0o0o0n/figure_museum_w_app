﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugPanel : MonoBehaviour 
{
	[SerializeField]
	private GameObject _container;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}




	public void OnCloseButtonClick()
	{
		Hide();
	}




	public void Show()
	{
		_container.gameObject.SetActive(true);
	}

	public void Hide()
	{
		_container.gameObject.SetActive(false);
	}
}
