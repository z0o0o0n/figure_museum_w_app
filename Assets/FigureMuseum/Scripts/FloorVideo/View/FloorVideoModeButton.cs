﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorVideoModeButton : MonoBehaviour 
{
	[SerializeField]
	private Text _textField;
	[SerializeField]
	private FloorVideoPlayer _videoPlayer;
	private bool _useFloorVideo = true;

	private void Start ()
	{
		_textField.text = "ON";
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
	}





	public void OnClick()
	{
		if(_useFloorVideo)
		{
			_useFloorVideo = false;
			_textField.text = "OFF";
			_videoPlayer.gameObject.SetActive(false);
		}
		else
		{
			_useFloorVideo = true;
			_textField.text = "ON";
			_videoPlayer.gameObject.SetActive(true);
		}
	}
}
