﻿using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorVideoPlayer : MonoBehaviour 
{
	[SerializeField]
	private ZoneService _zoneService;
	[SerializeField]
	private DisplayUGUI _videoDisplay;
	[SerializeField]
	private MediaPlayer _mediaPlayer;
	private bool _isPlaying = false;

	private void Awake()
	{
		//_zoneService.FloorChanged += OnFloorChanged;
		_zoneService.ZoneChanged += OnZoneChanged;

		_mediaPlayer.Events.AddListener(OnVideoEvent);
	}

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		//_zoneService.FloorChanged -= OnFloorChanged;
		_zoneService.ZoneChanged -= OnZoneChanged;

		_mediaPlayer.Events.RemoveListener(OnVideoEvent);
	}





	private void OnZoneChanged(string zoneId)
	{
		if (_isPlaying) return;

		if (zoneId == "25")
		{
			int floor = 2;
			PlayVideo(floor);
		}

		if (zoneId == "38")
		{
			int floor = 3;
			PlayVideo(floor);
		}

		if (zoneId == "42")
		{	
			int floor = 4;
			PlayVideo(floor);
		}

		if (zoneId == "50")
		{
			int floor = 5;
			PlayVideo(floor);
		}
	}

	private void PlayVideo(int floor)
	{
		_isPlaying = true;
		string videoPath = "Videos/FloorVideo" + floor + ".mp4";
		_mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToPeristentDataFolder, videoPath, true);
	}

	//private void OnFloorChanged(int floor)
	//{
	//	if (_isPlaying) return;
	//	if (floor <= 1 || floor > 5) return;

	//	_isPlaying = true;

	//	string videoPath = "Videos/FloorVideo" + floor + ".mp4";
	//	_mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToPeristentDataFolder, videoPath, true);
	//}

	private void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType e, ErrorCode errors)
	{
		switch (e)
		{
			case MediaPlayerEvent.EventType.ReadyToPlay:
				break;
			case MediaPlayerEvent.EventType.Started:
				break;
			case MediaPlayerEvent.EventType.FirstFrameReady:
				_videoDisplay.enabled = true;
				break;
			case MediaPlayerEvent.EventType.FinishedPlaying:
				_isPlaying = false;
				_videoDisplay.enabled = false;
				break;
		}
	}

}
