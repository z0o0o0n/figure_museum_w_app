﻿using DG.Tweening;
using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Intro : MonoBehaviour 
{
	public delegate void IntroEvent();
	public event IntroEvent IntroStart;
	public event IntroEvent IntroEnd;

	[SerializeField]
	private DisplayUGUI _videoDisplay;
	[SerializeField]
	private MediaPlayer _mediaPlayer;
	[SerializeField]
	private CanvasGroup _canvasGroup;
	[SerializeField]
	private Text _titleText;
	[SerializeField]
	private Button _okButton;
	private string _currentVideo;

	private void Start ()
	{
		_mediaPlayer.Events.AddListener(OnVideoEvent);

		_titleText.gameObject.SetActive(false);
		_titleText.DOFade(0f, 0f);
		_okButton.gameObject.SetActive(false);
		_okButton.GetComponent<Image>().DOFade(0f, 0f);

		PlayIntro();
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_mediaPlayer.Events.RemoveListener(OnVideoEvent);
	}




	private void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType e, ErrorCode errors)
	{
		switch (e)
		{
			case MediaPlayerEvent.EventType.ReadyToPlay:
				break;
			case MediaPlayerEvent.EventType.Started:
				break;
			case MediaPlayerEvent.EventType.FirstFrameReady:
				if(_currentVideo == "ViewingOrder")
				{
					_videoDisplay.DOFade(1f, 0.3f);
				}
				break;
			case MediaPlayerEvent.EventType.FinishedPlaying:
				if(_currentVideo == "Intro")
				{
					_videoDisplay.DOFade(0f, 0.3f);
					DOVirtual.DelayedCall(0.3f, PlayViewingOrder);
				}
				break;
		}
	}

	public void OnOkButtonClick()
	{
		Hide();
	}




	private void PlayIntro()
	{
		_currentVideo = "Intro";
		//DirectoryInfo videoPath = new DirectoryInfo(Application.persistentDataPath + "/Videos");
		string videoPath = "Videos/Intro_1" + ".mp4";
		_mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToPeristentDataFolder, videoPath, true);
	}

	private void PlayViewingOrder()
	{
		_currentVideo = "ViewingOrder";
		string videoPath = "Videos/ViewingOrder" + ".mp4";
		_mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToPeristentDataFolder, videoPath, true);
		_mediaPlayer.Control.SetLooping(true);
		_videoDisplay._scaleMode = ScaleMode.ScaleToFit;

		_titleText.gameObject.SetActive(true);
		_titleText.DOFade(1f, 0.5f);
		_okButton.gameObject.SetActive(true);
		_okButton.GetComponent<Image>().DOFade(1f, 0.5f);
	}


	private void Hide()
	{
		_mediaPlayer.Control.Stop();
		_canvasGroup.DOFade(0f, 0.3f).OnComplete(OnHided);
	}

	private void OnHided()
	{
		gameObject.SetActive(false);
	}
}
