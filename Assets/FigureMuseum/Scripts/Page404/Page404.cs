﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Page404 : MonoBehaviour 
{
	[SerializeField]
	private BeaconService _beaconService;
	[SerializeField]
	private CanvasGroup _page;

	private void Start ()
	{
		_beaconService.Empty += OnBeaconEmpty;
		_beaconService.Changed += OnBeaconChanged;
		_beaconService.ScanStoped += OnScanStoped;
	}
	
	private void Update () {
		
	}

	private void OnDestroy()
	{
		_beaconService.Empty -= OnBeaconEmpty;
		_beaconService.Changed -= OnBeaconChanged;
		_beaconService.ScanStoped -= OnScanStoped;
	}





	private void OnBeaconEmpty(BeaconSettingData beaconSettingData)
	{
		DOTween.Kill("page.fade." + _page.GetInstanceID());
		_page.gameObject.SetActive(true);
		_page.DOFade(1f, 0.4f).SetId("page.fade." + _page.GetInstanceID());
	}

	private void OnBeaconChanged(BeaconSettingData beaconSettingData)
	{
		DOTween.Kill("page.fade." + _page.GetInstanceID());
		_page.DOFade(0f, 0.4f).SetId("page.fade." + _page.GetInstanceID()).OnComplete(OnPageHided);
	}

	private void OnPageHided()
	{
		Hide();
	}

	private void OnScanStoped()
	{
		Hide();
	}




	private void Hide()
	{
		_page.gameObject.SetActive(false);
	}
}
