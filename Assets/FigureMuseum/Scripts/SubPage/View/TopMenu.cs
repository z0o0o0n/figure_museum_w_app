﻿using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopMenu : MonoBehaviour
{
	[SerializeField]
	private Image _bg;
	[SerializeField]
	private Image _dimBg;
	[SerializeField]
	private Text _locationInfoText;
	[SerializeField]
	private Button _backButton;
	[SerializeField]
	private Image _line;
	[SerializeField]
	private Image _shadow;

	private void Start()
	{

	}

	private void Update()
	{

	}





	public void SetData(GDEZoneData zoneData)
	{
		//_bg.color = zoneData.baseColor;
		//_line.color = zoneData.pointColor;
		//_locationInfoText.text = locationInfo;
	}

	public void Show()
	{
		_bg.gameObject.SetActive(true);
		_dimBg.gameObject.SetActive(true);
		_locationInfoText.gameObject.SetActive(true);
		_backButton.gameObject.SetActive(true);
		_line.gameObject.SetActive(true);
		_line.rectTransform.DOScaleX(1f, 1f).SetDelay(0.5f);
		_shadow.gameObject.SetActive(true);
	}

	public void Hide()
	{
	}

	public void Reset()
	{
		Debug.Log("reset");
		_bg.gameObject.SetActive(false);
		_dimBg.gameObject.SetActive(false);
		_locationInfoText.gameObject.SetActive(false);
		_backButton.gameObject.SetActive(false);
		_line.gameObject.SetActive(false);
		_line.rectTransform.DOScaleX(0f, 0f);
		_shadow.gameObject.SetActive(false);
	}
}
