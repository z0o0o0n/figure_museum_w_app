﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorItem : MonoBehaviour 
{
	[SerializeField]
	private Image _bg;
	[SerializeField]
	private Text _floorText;
	[SerializeField]
	private Text _nameText;
	[SerializeField]
	private Text _desc;
	[SerializeField]
	private Image _line;
	[SerializeField]
	private Color _bgOnColor;
	[SerializeField]
	private Color _bgOffColor;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
	
	}




	public void On()
	{
		_bg.color = _bgOnColor;
		_floorText.DOFade(1f, 0f);
		_nameText.DOFade(1f, 0f);
		_desc.DOFade(1f, 0f);
		_line.DOFade(1f, 0f);
	}

	public void Off()
	{
		_bg.color = _bgOffColor;
		_floorText.DOFade(0.3f, 0f);
		_nameText.DOFade(0.3f, 0f);
		_desc.DOFade(0.3f, 0f);
		_line.DOFade(0.3f, 0f);
	}
}
