﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorInfoPage : MonoBehaviour 
{
	[SerializeField]
	private List<FloorItem> _floorItemList;
	[SerializeField]
	private ZoneService _zoneService;

	private void Start ()
	{
		_zoneService.FloorChanged += OnFloorChanged;
	}

	private int _tempFloor = -1;
	private void Update ()
	{
		//if(Input.GetKeyUp(KeyCode.Q))
		//{
		//	SelectFloor(_tempFloor);
		//	_tempFloor++;

		//	if (_tempFloor > 5) _tempFloor = -1;
		//	else if (_tempFloor < -1) _tempFloor = 5;
		//}
	}

	private void OnEnable()
	{
		Debug.Log("current floor: " + _zoneService.GetCurrentFloor());
		SelectFloor(_zoneService.GetCurrentFloor());
	}

	private void OnDestroy()
	{
		_zoneService.FloorChanged -= OnFloorChanged;
	}





	private void OnFloorChanged(int floor)
	{
		SelectFloor(floor);
	}




	private void SelectFloor(int floor)
	{
		Debug.Log("select floor: " + floor);

		if (floor >= 5) floor = 5;
		else if (floor <= 0) floor = 0;

		for(int i = 0; i < _floorItemList.Count; i++)
		{
			if((floor) == i)
			{
				_floorItemList[i].On();
			}
			else
			{
				_floorItemList[i].Off();
			}
		}
	}
}
