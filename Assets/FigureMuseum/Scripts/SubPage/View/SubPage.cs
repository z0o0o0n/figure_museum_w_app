﻿using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubPage : MonoBehaviour
{
	[SerializeField]
	private ZoneService _zoneService;
	[SerializeField]
	private MainPage _mainPage;
	[SerializeField]
	private TopMenu _topMenu;
	[SerializeField]
	private Button _backButton;
	[SerializeField]
	private ScrollRect _scrollRect;
	[SerializeField]
	private GameObject _contentContainer;
	[SerializeField]
	private LoadingScreen _loadingScreen;
	[SerializeField]
	private List<GameObject> _subPageContentList;
	[SerializeField]
	private FigureService _figureService;

	private bool _isPrepared = false;
	private float _transitionDuration = 0.5f;
	private GameObject _currentSubContent;
	//private GDEZoneData _zoneData;
	private GDEFigureData _figureData;

	private void Awake()
	{
		_figureService.Prepared += OnPrepared;
		_figureService.DetailPageShow += OnDetailPageShow;
	}

	private void Start()
	{
		Reset();
	}

	private void Update()
	{
		//if (Input.GetKeyUp(KeyCode.Z))
		//{
		//	ShowSub("About");
		//}
		//else if (Input.GetKeyUp(KeyCode.X))
		//{
		//	ShowSub("FloorInfo");
		//}
		//else if (Input.GetKeyUp(KeyCode.C))
		//{
		//	ShowSub("ViewingOrder");
		//}
	}





	private void OnPrepared()
	{
	}

	private void OnDetailPageShow(string figureId)
	{
		if (int.Parse(figureId) <= 500) return;

		_figureData = _figureService.GetFigureData(figureId);

		string rawData = _figureData.contentList[0];
		string[] rawDatas = rawData.Split(new char[] { '|' });

		if (rawDatas[0] == "page")
		{
			ShowSub(rawDatas[1].ToString());
		}
	}

	public void OnBackButtonClick()
	{
		Hide();
	}

	public void OnFloorInfoButtonClick()
	{
		ShowSub("FloorInfo");
	}




	// Show
	// FloorInfo, About, ViewingOrder
	private void ShowSub(string id)
	{
		Debug.Log("ShowSub : " + id);
		//_zoneData = _zoneService.GetZoneDataByFigureId(figureId);

		//_loadingBg.color = _zoneData.baseColor;
		_loadingScreen.Open(_transitionDuration);
		//_loadingBg.rectTransform.DOSizeDelta(new Vector2(1080f, 1440f), _transitionDuration).SetEase(Ease.OutQuint).OnComplete(()=>OnBgOpenMotionEnd(id));

		_topMenu.SetData(null);

		DOVirtual.DelayedCall(_transitionDuration, ()=>OnBgOpenMotionEnd(id));
	}

	private void OnBgOpenMotionEnd(string id) // loading bg가 화면을 완전히 가렸을 때
	{
		_mainPage.Hide();
		_topMenu.Show();

		//_loadingBg.rectTransform.DOLocalMoveX(-540f, _transitionDuration).SetDelay(0.1f).SetEase(Ease.InQuint);

		_backButton.gameObject.SetActive(true);
		_backButton.GetComponent<Image>().DOFade(1f, 0.3f);

		_scrollRect.gameObject.SetActive(true);

		
		for(int i = 0; i < _subPageContentList.Count; i++)
		{
			_subPageContentList[i].gameObject.SetActive(false);
		}

		if ("About" == id)
		{
			_contentContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, _subPageContentList[0].GetComponent<RectTransform>().sizeDelta.y);
			_subPageContentList[0].gameObject.SetActive(true);
		}
		else if ("FloorInfo" == id)
		{
			_contentContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, _subPageContentList[1].GetComponent<RectTransform>().sizeDelta.y);
			_subPageContentList[1].gameObject.SetActive(true);
		}
		else if ("ViewingOrder" == id)
		{
			_contentContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, _subPageContentList[2].GetComponent<RectTransform>().sizeDelta.y);
			_subPageContentList[2].gameObject.SetActive(true);
		}
	}

	// Hide
	private void Hide()
	{
		_backButton.gameObject.SetActive(false);
		_topMenu.Hide();

		_loadingScreen.Close(_transitionDuration);
		//_loadingBg.rectTransform.DOLocalMoveX(540f, _transitionDuration).SetEase(Ease.InQuint).OnComplete(OnBgCloseMotionEnd);

		DOVirtual.DelayedCall(_transitionDuration, OnBgCloseMotionEnd);
	}

	private void OnBgCloseMotionEnd()
	{
		_mainPage.Show();
		_scrollRect.gameObject.SetActive(false);

		//_loadingBg.rectTransform.DOSizeDelta(new Vector2(0f, 1440f), _transitionDuration).SetDelay(0.1f).SetEase(Ease.InQuint);

		_topMenu.Reset();
	}

	private void OnHided()
	{
		Reset();
	}

	// Reset
	private void Reset()
	{
		_loadingScreen.Reset();
		//_loadingBg.rectTransform.DOSizeDelta(new Vector2(0f, 1440f), 0f);
		//_loadingBg.rectTransform.DOLocalMoveY(0f, 0f);

		_backButton.gameObject.SetActive(false);
		_backButton.GetComponent<Image>().DOFade(0f, 0f);

		_scrollRect.gameObject.SetActive(false);

		_topMenu.Reset();
	}





	public void Show(string figureId)
	{
		OnDetailPageShow(figureId);
	}
}
