﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureViewer : MonoBehaviour
{
	public delegate void FigureViewerEvent(string id);
	public event FigureViewerEvent Changed;

	[SerializeField]
	private Camera _camera;
	[SerializeField]
	private Light _light;
	[SerializeField]
	private Material _bgMaterial;
	[SerializeField]
	private List<Texture> _bgTextureList;
	[SerializeField]
	private FigureAngleIndicator _angleIndicator;

	//private string _figure0Path = "Deadpool/DeadpoolModel";
	//private float _deadpoolStartYpos = -0.18f;
	//private float _deadpoolStartYrot = -90f;
	//private float _deadpoolLighting = 1.2f;
	////private string _figure1Path = "Rupi/RupiModel";
	//private string _figure1Path = "Girl/cha_GirlA_mod_v01";
	//private float _girlStartYpos = -0.14f;
	//private float _girlLighting = 1.2f;

	//private string _figure2Path = "RupiUnlit/RupiUnlitModel";
	//private float _rupiStartYpos = 0f;
	//private float _rupiStartYrot = 180f;
	//private float _rupiLighting = 0f;

	//private string _figure3Path = "SuperGirl/SuperGirlNoRock";
	//private float _superGirlStartYpos = -0.2f;
	//private float _superGirlYrot = 0f;
	//private float _superGirlLighting = 1f;

	private GameObject _currentFigure;

	void Start ()
    {
	}
	
	void Update ()
    {
		_angleIndicator.SetAngle(transform.localEulerAngles.y);
	}





	private void LoadFBX(string id, string path, float yPos, float yRot)
	{
		float startTime = Time.realtimeSinceStartup;
		_currentFigure = Instantiate(Resources.Load(path)) as GameObject;
		Debug.Log("loading time: " + (Time.realtimeSinceStartup - startTime));
		_currentFigure.transform.parent = transform;
		_currentFigure.transform.localScale = Vector3.one;
		_currentFigure.transform.localPosition = new Vector3(0f, yPos, 0f);
		_currentFigure.transform.localEulerAngles = new Vector3(0f, yRot, 0f);

		if (Changed != null) Changed(id);
	}

	private void TurnOffLight()
	{
		//_light.enabled = false;
	}

	private void TurnOnLight()
	{
		//_light.enabled = true;
	}





	public void Show()
	{
		Reset();
		DOTween.Kill("LocalRotation." + GetInstanceID());
		gameObject.transform.DOLocalRotate(new Vector3(0f, 360f, 0f), 2f, RotateMode.FastBeyond360).SetId("LocalRotation." + GetInstanceID()).SetEase(Ease.OutCubic);
	}

	public void Hide()
	{
		Vector3 currentRotation = gameObject.transform.localEulerAngles;
		currentRotation.y -= 360f;
		DOTween.Kill("LocalRotation." + GetInstanceID());
		gameObject.transform.DOLocalRotate(currentRotation, 1f, RotateMode.FastBeyond360).SetId("LocalRotation." + GetInstanceID()).SetEase(Ease.InCubic);
	}

	private void Reset()
	{
		//DOTween.Kill("LocalRotation." + GetInstanceID());
		gameObject.transform.localEulerAngles = Vector3.zero;
		//gameObject.transform.DOLocalRotate(new Vector3(0f, 0f, 0f), 0f, RotateMode.FastBeyond360).SetId("LocalRotation." + GetInstanceID());
		_camera.transform.DOLocalMove(new Vector3(0f, 25f, -90), 0f);
	}

	//public void ChangeFigure(string figureId)
	//{
	//	switch(figureId)
	//	{
	//		case "0":
	//			TurnOnLight();
	//			Destroy(_currentFigure);
	//			LoadFBX(figureId, _figure0Path, _deadpoolStartYpos, _deadpoolStartYrot);
	//			_light.intensity = _deadpoolLighting;
	//			_bgMaterial.mainTexture = _bgTextureList[2];
	//			break;
	//		case "1":
	//			TurnOnLight();
	//			Destroy(_currentFigure);
	//			LoadFBX(figureId, _figure1Path, _girlStartYpos, _rupiStartYrot);
	//			_light.intensity = _girlLighting;
	//			_bgMaterial.mainTexture = _bgTextureList[0];
	//			break;
	//		case "2":
	//			TurnOffLight();
	//			Destroy(_currentFigure);
	//			LoadFBX(figureId, _figure2Path, _rupiStartYpos, _rupiStartYrot);
	//			_light.intensity = _rupiLighting;
	//			_bgMaterial.mainTexture = _bgTextureList[1];
	//			break;
	//		case "3":
	//			TurnOnLight();
	//			Destroy(_currentFigure);
	//			LoadFBX(figureId, _figure3Path, _superGirlStartYpos, _superGirlYrot);
	//			_light.intensity = _superGirlLighting;
	//			_bgMaterial.mainTexture = _bgTextureList[3];
	//			break;
	//	}
	//}
}
