﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FigureThumbnail : MonoBehaviour 
{
	public delegate void FigureThumbnailEvent(string figureId);
	public event FigureThumbnailEvent Click;

	[SerializeField]
	private Image _bgImage;
	[SerializeField]
	private Image _image;
	[SerializeField]
	private Text _nameText;
	[SerializeField]
	private Text _scaleInfoText;
	[SerializeField]
	private Image _scaleInfoOutlineImage;
	[SerializeField]
	private Image _lineEffectBottomImage;

	private string _figureId = "";

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	public void OnThumbnailClick()
	{
		if (_figureId == "")
		{
			Debug.Log("해당 Thumbnail에 figureId 값이 없습니다.");
			return;
		}

		if (Click != null) Click(_figureId);
	}





	public void SetFigureData(string figureId, string name, Sprite image, Color bgColor, Color pointColor)
	{
		_figureId = figureId;

		bgColor.a = 0;
		_nameText.DOFade(0f, 0f);
		_nameText.rectTransform.DOLocalMoveY(-150f - 15f, 0f);
		_image.DOFade(0f, 0f);
		_image.rectTransform.DOScale(1.1f, 0f);
		_scaleInfoText.DOFade(0f, 0f);
		_scaleInfoText.rectTransform.DOLocalMoveY(-200f - 15f, 0f);
		_scaleInfoOutlineImage.DOFade(0f, 0f);
		_scaleInfoOutlineImage.rectTransform.DOLocalMoveY(-200f - 15f, 0f);

		_nameText.text = name;
		_image.sprite = image;
		_bgImage.color = bgColor;
		_lineEffectBottomImage.color = pointColor;
	}

	public void Show()
	{
		_bgImage.DOFade(1f, 0.25f);

		_lineEffectBottomImage.rectTransform.DOSizeDelta(new Vector2(534f, 2f), 0.25f).SetEase(Ease.InCubic).OnComplete(Test);
	}

	private void Test()
	{
		_lineEffectBottomImage.rectTransform.localPosition = new Vector2(267f, -266f);
		_lineEffectBottomImage.rectTransform.pivot = new Vector2(1f, 0.5f);
		_lineEffectBottomImage.rectTransform.DOSizeDelta(new Vector2(0f, 2f), 0.25f).SetEase(Ease.OutCubic);

		_image.DOFade(1f, 0.25f);
		_image.rectTransform.DOScale(1f, 0.5f).SetEase(Ease.OutCubic);

		_nameText.DOFade(1f, 0.25f);
		_nameText.rectTransform.DOLocalMoveY(-150f, 0.3f).SetEase(Ease.OutCubic);
		_scaleInfoText.DOFade(1f, 0.25f).SetDelay(0.1f);
		_scaleInfoText.rectTransform.DOLocalMoveY(-200f, 0.3f).SetDelay(0.1f).SetEase(Ease.OutCubic);
		_scaleInfoOutlineImage.DOFade(1f, 0.25f).SetDelay(0.1f);
		_scaleInfoOutlineImage.rectTransform.DOLocalMoveY(-200f, 0.3f).SetDelay(0.1f).SetEase(Ease.OutCubic);
	}
}
