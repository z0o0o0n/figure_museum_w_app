﻿using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoContent : MonoBehaviour 
{
	[SerializeField]
	private MediaPlayer _mediaPlayer;
	private bool _isPlaying = false;

	private void Start ()
	{
		_mediaPlayer.Events.AddListener(OnVideoEvent);
	}
	
	private void Update ()
	{
		if(transform.position.y > 1550f || transform.position.y < 300f)
		{
			if(_isPlaying) Stop();
		}
		else
		{
			if (!_isPlaying) Play();
		}
		//Debug.Log(transform.position.y);
	}

	private void OnDestroy()
	{
		_mediaPlayer.Events.RemoveListener(OnVideoEvent);
	}





	private void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType e, ErrorCode errors)
	{
		switch (e)
		{
			case MediaPlayerEvent.EventType.ReadyToPlay:
				break;
			case MediaPlayerEvent.EventType.Started:
				break;
			case MediaPlayerEvent.EventType.FirstFrameReady:
				break;
			case MediaPlayerEvent.EventType.FinishedPlaying:
				break;
		}
	}

	private void Play()
	{
		_isPlaying = true;
		_mediaPlayer.Play();
	}

	private void Stop()
	{
		_isPlaying = false;
		_mediaPlayer.Stop();
	}




	public void Init(string videoClipId)
	{
		_mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToPeristentDataFolder, "Videos/Detail_" + videoClipId + ".mp4", false);
	}
}
