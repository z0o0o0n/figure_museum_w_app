﻿using DG.Tweening;
using Junhee.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FigureAngleIndicator : MonoBehaviour 
{
	[SerializeField]
	private Image _angleCircle;
	[SerializeField]
	private Image _angleSign;
	[SerializeField]
	private Text _angleText;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	public void SetColor(Color pointColor)
	{
		_angleCircle.color = pointColor;
		_angleSign.color = pointColor;
		_angleText.color = pointColor;
	}

	public void SetAngle(float angle)
	{
		int angleInt = Mathf.RoundToInt(angle);
		if (angleInt == 0)
		{
			angleInt = 360;
			angle = 360;
		}
		_angleText.text = Format.ConvertDigit(angleInt.ToString(), 3, "0");
		_angleCircle.fillAmount = angle / 360;
		_angleSign.rectTransform.DOLocalMoveX(_angleText.preferredWidth / 2 + 4, 0f);
	}
}
