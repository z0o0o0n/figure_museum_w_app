﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class FPSViewer : MonoBehaviour
{
	[SerializeField]
	private FigureViewer _figureViewer;

    private float distancePerSecond = 60f;
    private Text fpsText;
    private float timeA;
    private int fps;
    private int lastFPS;
    private int verts;
    private int tris;

    private void Awake()
    {
		_figureViewer.Changed += OnFigureChanged;

		fpsText = GetComponent<Text>();
        timeA = Time.timeSinceLevelLoad;
        DontDestroyOnLoad(this);
		UpdateObjectStats();
    }

    void Update()
    {
        if (Time.timeSinceLevelLoad - timeA <= 1)
        {
            fps++;            
        }
        else
        {
            lastFPS = fps + 1;
            timeA = Time.timeSinceLevelLoad;
            fps = 0;
        }

        string log = "FPS: " + lastFPS.ToString() + "\n";
        log += "Vertex: " + verts.ToString() + "\n";
        log += "Tris: " + tris.ToString();
        //fpsText.text = lastFPS.ToString();
        fpsText.text = log;
    }

	private void OnDestroy()
	{
		_figureViewer.Changed -= OnFigureChanged;
	}





	private void OnFigureChanged(string figureId)
	{
		Debug.Log("OnFigureChanged: " + figureId);
		DOTween.Kill("ObjectStatsUpdateDelay" + GetInstanceID());
		DOVirtual.DelayedCall(0.2f, UpdateObjectStats).SetId("ObjectStatsUpdateDelay" + GetInstanceID());
	}





	private void UpdateObjectStats()
    {
        verts = 0;
        tris = 0;
        GameObject[] ob = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach (GameObject obj in ob)
        {
            GetObjectStats(obj);
        }
    }

    private void GetObjectStats(GameObject obj)
    {
        Component[] filters;
        filters = obj.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter f in filters)
        {
            tris += f.sharedMesh.triangles.Length / 3;
            verts += f.sharedMesh.vertexCount;
        }
    }
}