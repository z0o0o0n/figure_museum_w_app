﻿using DG.Tweening;
using GameDataEditor;
using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FigureDetailScrollView : MonoBehaviour 
{
	[SerializeField]
	private Image _contentBg;
	[SerializeField]
	private Image _dimBg;
	[SerializeField]
	private RectTransform _contentContainerRect;
	[SerializeField]
	private Image _scrollBarBg;
	[SerializeField]
	private Image _scrollBarHandler;
	[SerializeField]
	private GameObject _textContentPrefab;
	[SerializeField]
	private GameObject _imageContentPrefab;
	[SerializeField]
	private GameObject _videoContentPrefab;
	[SerializeField]
	private Text _zoneNameText;
	[SerializeField]
	private Image _titleLine;
	[SerializeField]
	private Text _figureNameText;
	private GDEFigureData _currentFigureData;
	private List<GameObject> _contentList;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
	}





	private void Create()
	{
		Debug.Log("content count: " + _currentFigureData.contentList.Count);

		_contentList = new List<GameObject>();

		float startPosY = 1100f;
		float marginTop = 240f;
		float marginBottom = 80f;
		float spaceY = 50f;
		float height = 0;
		for (int i = 0; i < _currentFigureData.contentList.Count; i++)
		{
			string rawData = _currentFigureData.contentList[i];
			Debug.Log("rawData" + i + ": " + rawData);
			string[] rawDatas = rawData.Split(new char[] { '|' });

			GameObject instance = null;
			if(rawDatas[0] == "img")
			{
				int imageIndex = int.Parse(rawDatas[1]);

				Texture imageTexture = Resources.Load("FigureImageContents/Figure_" + _currentFigureData.id + "_" + imageIndex.ToString()) as Texture;
				Debug.Log("texture: " + imageTexture);

				instance = Instantiate(_imageContentPrefab);
				RawImage imageContent = instance.GetComponent<RawImage>();
				imageContent.rectTransform.SetParent(_contentContainerRect.transform);
				imageContent.rectTransform.localScale = Vector3.one;
				imageContent.rectTransform.localPosition = new Vector3(540f, (-startPosY - marginTop) - height, 0f);
				imageContent.texture = imageTexture;
				imageContent.rectTransform.sizeDelta = new Vector2(1080, imageTexture.height);

				height += (imageContent.rectTransform.sizeDelta.y + spaceY);
				Debug.Log(i + " image" + " / name: " + "Figure_" + _currentFigureData.id + "_" + imageIndex.ToString() + " / height: " + height);
			}
			else if(rawDatas[0] == "text")
			{
				string text = rawDatas[1];

				instance = Instantiate(_textContentPrefab);
				Text textContent = instance.GetComponent<Text>();
				textContent.rectTransform.SetParent(_contentContainerRect.transform);
				textContent.rectTransform.localScale = Vector3.one;
				textContent.rectTransform.localPosition = new Vector3(540f, (-startPosY - marginTop) - height, 0f);
				textContent.text = text;
				textContent.rectTransform.sizeDelta = new Vector2(960f, textContent.preferredHeight);
				height += (textContent.preferredHeight + spaceY);
				Debug.Log(i + " text /" + " height: " + height);
			}
			else if (rawDatas[0] == "video")
			{
				string videoClipId = rawDatas[1];

				instance = Instantiate(_videoContentPrefab);
				RectTransform display = instance.GetComponent<RectTransform>();
				display.SetParent(_contentContainerRect.transform);
				display.localScale = Vector3.one;
				display.localPosition = new Vector3(540f, (-startPosY - marginTop) - height, 0f);
				display.sizeDelta = new Vector2(1080, 590);
				height += (590 + spaceY);

				VideoContent videoContent = instance.GetComponent<VideoContent>();
				videoContent.Init(videoClipId);
				//textContent.text = text;
				//textContent.rectTransform.sizeDelta = new Vector2(960f, textContent.preferredHeight);
				//height += (textContent.preferredHeight + spaceY);
				//Debug.Log(i + " text /" + " height: " + height);
			}

			if (instance != null)_contentList.Add(instance);
		}
		float contentHeight = (height - spaceY) + marginTop + marginBottom;
		_contentBg.rectTransform.sizeDelta = new Vector2(1080, contentHeight);
		_dimBg.rectTransform.sizeDelta = new Vector2(1080, contentHeight);
		_contentContainerRect.sizeDelta = new Vector2(0, contentHeight + startPosY);
	}





	public void CreateContent(GDEZoneData zoneData, GDEFigureData figureData)
	{
		Color bgColor = zoneData.baseColor;
		bgColor.a = 0.8f;
		_contentBg.color = bgColor;

		_scrollBarHandler.color = zoneData.pointColor;

		_zoneNameText.color = zoneData.pointColor;
		_zoneNameText.text = zoneData.name;

		_figureNameText.text = figureData.name;

		_titleLine.color = zoneData.pointColor;
		//_titleLine.rectTransform.localPosition = new Vector3(_f)

		Color baseColor = zoneData.baseColor - new Color(0.2f, 0.2f, 0.2f, 0f);
		_scrollBarBg.color = baseColor;

		_currentFigureData = figureData;

		Create();
	}

	public void Reset()
	{
		_contentContainerRect.DOLocalMoveY(0f, 0f);

		if(_contentList.Count > 0)
		{
			for(int i = 0; i < _contentList.Count; i++)
			{
				Destroy(_contentList[i]);
			}
			_contentList.Clear();
		}

		Resources.UnloadUnusedAssets();
	}
}
