﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureAccelerationContainer : MonoBehaviour 
{
	private float _accelerationY = 0f;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		// acce 수직으로 세웠을 때 -1 앞쪽으로 90도 뉘였을 때 1
		// acce 가 -1 일때 피규어 회전 각은 -50, 1 일때 50
		
#if UNITY_ANDROID && !UNITY_EDITOR
		_accelerationY = Input.acceleration.y * -1;
		if(_accelerationY <= 0) _accelerationY = 0;
		//_accelerationY = 1 - _accelerationY;
#elif UNITY_EDITOR
		if (Input.GetKeyUp(KeyCode.A))
		{
			_accelerationY += 0.02f;
		}
		else if (Input.GetKeyUp(KeyCode.S))
		{
			_accelerationY -= 0.02f;
		}

		if (_accelerationY >= 1f) _accelerationY = 1f;
		else if (_accelerationY <= 0f) _accelerationY = 0f;
#endif
		//angles.x += (targetAngle - angles.x) * 0.3f;
		//transform.localEulerAngles = angles;

		Vector3 angles = transform.localEulerAngles;
		float targetAngle = (_accelerationY * 120f) - 60f;
		angles.x = targetAngle;
		transform.DOLocalRotate(angles, 0.5f).SetEase(Ease.Linear);
	}
}
