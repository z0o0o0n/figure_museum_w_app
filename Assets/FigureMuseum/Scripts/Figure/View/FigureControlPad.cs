﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using TouchScript;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;

public class FigureControlPad : MonoBehaviour
{
    [SerializeField]
    private Camera _mainCamera;
    [SerializeField]
    private GameObject _centerPoint;
    [SerializeField]
    private GameObject _model;
    private Image _padImage;
    private MetaGesture _metaGesture;
    private List<TouchPoint> _touchPoints = new List<TouchPoint>();
    private Vector2 _startPos = Vector2.zero;
    private Vector2 _offsetPos = Vector2.zero;
    private Vector3 _startRotation = Vector3.zero;
    private float _startDistance = 0f;
    private float _offsetDistance = 0f;
    private float _minZoom = -90f;
    private float _maxZoom = -40;
    private float _minX = -15f;
    private float _maxX = 15f;
    private float _minY = 0f;
    private float _maxY = 40f;
    private Vector3 _startCameraPos = Vector3.zero;
    private Vector2 _startCenterPos = Vector2.zero;
    private Vector2 _offsetCenterPos = Vector2.zero;

    private void Awake()
    {
        _metaGesture = GetComponent<MetaGesture>();
        _metaGesture.TouchBegan += OnTouchBegan;
        _metaGesture.TouchMoved += OnTouchMoved;
        _metaGesture.TouchEnded += OnTouchEnded;

        _padImage = GetComponent<Image>();
        _padImage.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
    }

    private void Start ()
    {
        
    }
	
	private void Update ()
    {

	}





    private void OnTouchBegan(object sender, MetaGestureEventArgs e)
    {
        _touchPoints.Add(e.Touch);

        //string log = "began: ";
        //for (int i = 0; i < _touchPoints.Count; i++)
        //{
        //    log += _touchPoints[i].Id + " / ";
        //}
        //Debug.Log(log);

        if(_touchPoints.Count == 1)
        {
            _startPos = _touchPoints[0].Position;
            _startRotation = _model.transform.localEulerAngles;
        }
        else if(_touchPoints.Count == 2)
        {
            _startDistance = Vector2.Distance(_touchPoints[0].Position, _touchPoints[1].Position);
            _startCameraPos = _mainCamera.transform.localPosition;
            _startCenterPos = GetCenter(_touchPoints[0].Position, _touchPoints[1].Position);
            _centerPoint.transform.localPosition = _startCenterPos;
        }
    }

    private void OnTouchMoved(object sender, MetaGestureEventArgs e)
    {
        if(_touchPoints.Count == 1)
        {
			// Rotate
            _offsetPos = _touchPoints[0].Position - _startPos;
            Vector3 modelRotation = _model.transform.localEulerAngles;
            modelRotation.y = _startRotation.y - (_offsetPos.x / 1.8f);
            
            _model.transform.DOLocalRotate(modelRotation, 0.1f).SetEase(Ease.OutSine);
        }
        else if(_touchPoints.Count == 2)
        {
			// Move
            _offsetDistance = Vector2.Distance(_touchPoints[0].Position, _touchPoints[1].Position) - _startDistance;
            _offsetCenterPos = GetCenter(_touchPoints[0].Position, _touchPoints[1].Position) - _startCenterPos;

            Vector3 cameraPos = _mainCamera.transform.localPosition;
            cameraPos.x = _startCameraPos.x - (_offsetCenterPos.x / 20);
            if (cameraPos.x < _minX) cameraPos.x = _minX;
            else if (cameraPos.x > _maxX) cameraPos.x = _maxX;
            cameraPos.y = _startCameraPos.y - (_offsetCenterPos.y / 20);
            if (cameraPos.y < _minY) cameraPos.y = _minY;
            else if (cameraPos.y > _maxY) cameraPos.y = _maxY;
            cameraPos.z = _startCameraPos.z + (_offsetDistance / 20);
            if (cameraPos.z < _minZoom) cameraPos.z = _minZoom;
            else if (cameraPos.z > _maxZoom) cameraPos.z = _maxZoom;

            _mainCamera.transform.DOLocalMove(cameraPos, 0.05f).SetEase(Ease.OutSine);
            //_mainCamera.transform.localPosition = cameraPos;

            _centerPoint.transform.localPosition = GetCenter(_touchPoints[0].Position, _touchPoints[1].Position);
        }
    }

    private void OnTouchEnded(object sender, MetaGestureEventArgs e)
    {
        _touchPoints.Remove(e.Touch);

        if (_touchPoints.Count == 1)
        {
            _startPos = _touchPoints[0].Position;
            _startRotation = _model.transform.localEulerAngles;
        }

        //string log = "ended: ";
        //for (int i = 0; i < _touchPoints.Count; i++)
        //{
        //    log += _touchPoints[i].Id + " / ";
        //}
        //Debug.Log(log);
    }

    private Vector2 GetCenter(Vector2 a, Vector2 b)
    {
        float centerX = (a.x - ((a.x - b.x) / 2)) - (Screen.width / 2);
        float centerY = (a.y - ((a.y - b.y) / 2)) - (Screen.height / 2);
        return new Vector2(centerX, centerY);
    }
}
