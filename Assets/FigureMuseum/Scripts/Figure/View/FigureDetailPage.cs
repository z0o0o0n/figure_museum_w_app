﻿using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FigureDetailPage : MonoBehaviour
{
	[SerializeField]
	private ZoneService _zoneService;
	[SerializeField]
	private FigureService _figureServcie;
	[SerializeField]
	private Image _backButtonImage;
	//[SerializeField]
	//private Image _loadingBg;
	[SerializeField]
	private LoadingScreen _loadingScreen;
	[SerializeField]
	private MainPage _mainPage;
	[SerializeField]
	private DetailpageTopMenu _topMenu;
	[SerializeField]
	private FigureControlPad _figureControlPad;
	[SerializeField]
	private FigureDetailScrollView _figureDetailScrollView;
	[SerializeField]
	private FigureViewer _figureViewer;
	[SerializeField]
	private Shader _modelShader;
	[SerializeField]
	private FigureAngleIndicator _angleIndicator;
	private string _currentFigureId = "";
	private string _figureFilename = "";
	private float _transitionDuration = 0.5f;
	private GameObject _currentFigureModelInstance;


	private bool _isPrepared = false;
	private GDEZoneData _zoneData;
	private GDEFigureData _figureData;

	private void Awake()
	{
		Reset();

		_figureServcie.Prepared += OnPrepared;
		_figureServcie.DetailPageShow += OnDetailPageShow;
		if (_figureServcie.isPrepared) OnPrepared();
	}

	private void Start()
	{

	}

	private int _index = 1;
	private void Update()
	{
		//if (Input.GetKeyUp(KeyCode.D))
		//{
		//	ShowFigure(_index.ToString());
		//	_index++;
		//}
		//if (Input.GetKeyUp(KeyCode.S))
		//{
		//	Hide();
		//}

		if(Input.GetKeyUp(KeyCode.Space))
		{
			ShowFigure(_index.ToString());
			_index++;
		}
	}





	private void OnPrepared()
	{
		if (!_figureServcie.isPrepared) return;

		_isPrepared = true;
	}

	private void OnDetailPageShow(string figureId)
	{
		ShowFigure(figureId);
	}

	public void OnBackButtonClick()
	{
		Hide();
	}





	private void ShowFigure(string figureId)
	{
		if (!_isPrepared)
		{
			Debug.Log("Figure Detail Page가 아직 준비되지 않았습니다.");
			return;
		}

		if (int.Parse(figureId) > 500) return;

		Debug.Log("figureId: " + figureId);

		_zoneData = _zoneService.GetZoneDataByFigureId(figureId);
		_figureData = _figureServcie.GetFigureData(figureId);
		_currentFigureId = figureId;

		if (_figureData == null)
		{
			Debug.LogWarning("figure data가 없음");
			return;
		}

		if (_zoneData == null)
		{
			Debug.LogWarning("zone data가 없음");
			return;
		}

		_loadingScreen.SetColor(_zoneData.baseColor, _zoneData.pointColor);
		_loadingScreen.Open(_transitionDuration);
		//_loadingBg.color = _zoneData.baseColor;
		//_loadingBg.rectTransform.DOSizeDelta(new Vector2(1080f, 1440f), _transitionDuration).SetEase(Ease.OutQuint).OnComplete(OnBgOpenMotionEnd);

		_backButtonImage.gameObject.SetActive(true);
		_backButtonImage.DOFade(1f, 0.3f);

		_angleIndicator.SetColor(_zoneData.pointColor);

		_topMenu.SetData(_zoneData);

		DOVirtual.DelayedCall(_transitionDuration, OnBgOpenMotionEnd);
	}

	private void OnBgOpenMotionEnd()
	{
		string figureFilename = _figureServcie.GetFigureFilenameById(_currentFigureId);

		_mainPage.Hide();
		_figureControlPad.gameObject.SetActive(true);
		_figureDetailScrollView.CreateContent(_zoneData, _figureData);
		_figureDetailScrollView.gameObject.SetActive(true);

		//_loadingBg.rectTransform.DOLocalMoveX(-540f, _transitionDuration).SetDelay(0.1f).SetEase(Ease.InQuint);
		//_loadingBg.rectTransform.DOLocalMoveY(-800f, 0.3f).SetEase(Ease.InOutExpo);

		_figureViewer.gameObject.SetActive(true);
		_figureViewer.Show();

		_topMenu.Show();

		LoadFigureModel();

		_angleIndicator.gameObject.SetActive(true);
	}

	private void LoadFigureModel()
	{
		if (_currentFigureModelInstance != null) Destroy(_currentFigureModelInstance);

		_figureFilename = _figureServcie.GetFigureFilenameById(_figureData.id);
		if (_figureFilename == null)
		{
			Debug.Log("figure filename can not be null");
			return;
		}
		Debug.Log("figureId: " + _figureData.id + " / filename: " + _figureFilename);
		StartCoroutine(LoadPrefab(_figureServcie.GetFigureAsssetBundle(), _figureFilename));
	}

	private IEnumerator LoadPrefab(AssetBundle assetBundle, string assetName)
	{
		AssetBundleRequest request = assetBundle.LoadAssetAsync(assetName);
		yield return request;

		TraceBox.Log("request: " + request.asset);

		GameObject prefab = request.asset as GameObject;
		_currentFigureModelInstance = Instantiate(prefab);
		_currentFigureModelInstance.transform.SetParent(_figureViewer.transform);
		_currentFigureModelInstance.transform.localPosition = Vector3.zero;
		_currentFigureModelInstance.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
		_currentFigureModelInstance.transform.localScale = new Vector3(10f, 10f, 10f);

		// change shadow
		Component[] meshRenderers = _currentFigureModelInstance.GetComponentsInChildren<MeshRenderer>();
		foreach (MeshRenderer mr in meshRenderers)
		{
			mr.material.shader = _modelShader;
		}
	}

	private void Hide()
	{
		//_loadingBg.rectTransform.DOLocalMoveX(540f, _transitionDuration).SetEase(Ease.InQuint).OnComplete(OnBgCloseMotionEnd);
		//_loadingBg.rectTransform.DOSizeDelta(new Vector2(0f, 1440f), 0.3f).SetEase(Ease.OutExpo).OnComplete(OnHided);
		_loadingScreen.Close(_transitionDuration);

		_backButtonImage.gameObject.SetActive(false);
		_figureViewer.Hide();
		_topMenu.Hide();

		DOVirtual.DelayedCall(_transitionDuration, OnBgCloseMotionEnd);
	}

	private void OnBgCloseMotionEnd()
	{
		_mainPage.Show();
		_figureControlPad.gameObject.SetActive(false);
		_figureDetailScrollView.Reset();
		_figureDetailScrollView.gameObject.SetActive(false);

		_figureViewer.gameObject.SetActive(false);

		//_loadingBg.rectTransform.DOSizeDelta(new Vector2(0f, 1440f), _transitionDuration).SetDelay(0.1f).SetEase(Ease.InQuint);

		_angleIndicator.gameObject.SetActive(false);

		_topMenu.Reset();
	}

	private void OnHided()
	{
		Reset();
	}

	private void Reset()
	{
		_loadingScreen.Reset();

		_backButtonImage.gameObject.SetActive(false);
		_backButtonImage.DOFade(0f, 0f);

		_topMenu.Reset();
	}
}
