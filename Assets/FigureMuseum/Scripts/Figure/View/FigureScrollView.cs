﻿using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FigureScrollView : MonoBehaviour 
{
	[SerializeField]
	private Image _contentBgImage;
	[SerializeField]
	private Image _scrollHandleImage;
	[SerializeField]
	private FigureService _figureService;
	[SerializeField]
	private ZoneService _zoneService;
	[SerializeField]
	private GameObject _figureThumbnailPrefab;
	[SerializeField]
	private RectTransform _contentContainerRect;
	[SerializeField]
	private GameObject _textBoxPrefab;
	[SerializeField]
	private GameObject _imageBoxPrefab;
	private List<FigureThumbnail> _figureThumbnailList = new List<FigureThumbnail>();

	private void Awake()
	{
		_figureService.Prepared += OnPrepared;
		if (_figureService.isPrepared) OnPrepared();

		_zoneService.Prepared += OnPrepared;
		if (_zoneService.isPrepared) OnPrepared();
	}

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_zoneService.ZoneChanged -= OnZoneChanged;
	}





	private void Init()
	{
		_zoneService.ZoneChanged += OnZoneChanged;
	}

	private void UpdateList(string zoneId)
	{
		if(_figureThumbnailList.Count > 0)
		{
			for(int i = 0; i < _figureThumbnailList.Count; i++)
			{
				_figureThumbnailList[i].Click -= OnThumbnailClick;
				Destroy(_figureThumbnailList[i].gameObject);
			}
			_figureThumbnailList.Clear();
		}

		
		List<GDEFigureData> figureDataList = _figureService.GetFigureDataListByZoneId(zoneId);
		GDEZoneData zoneData = _zoneService.GetZoneDataByZoneId(zoneId);

		// content container height값 설정
		int totalLineCount = 1;
		if (figureDataList.Count % 2 == 0)
		{
			totalLineCount = (figureDataList.Count / 2);
		}
		else
		{
			totalLineCount = ((figureDataList.Count / 2) + 1);
		}
		Vector2 contentContainerSize = _contentContainerRect.sizeDelta;
		contentContainerSize.y = totalLineCount * 540f;
		_contentContainerRect.sizeDelta = contentContainerSize;

		Vector2 contentContainerPos = _contentContainerRect.localPosition;
		contentContainerPos.y = 0f;
		_contentContainerRect.localPosition = contentContainerPos;

		int lineCount = 0;
		for (int i = 0; i < figureDataList.Count; i++)
		{
			GameObject instance = Instantiate(_figureThumbnailPrefab);
			instance.transform.SetParent(_contentContainerRect);
			instance.transform.localScale = Vector2.one;

			if(i % 2 == 0)
			{
				instance.transform.localPosition = new Vector2(275f, -270f - (lineCount * 538f));
			}
			else
			{
				instance.transform.localPosition = new Vector2(813f, -270f - (lineCount * 538f));
				lineCount++;
			}

			Sprite thumbnailSprite = _figureService.GetFigureThumbnail(figureDataList[i].id);

			FigureThumbnail figureThumbnail = instance.GetComponent<FigureThumbnail>();
			figureThumbnail.SetFigureData(figureDataList[i].id, figureDataList[i].name, thumbnailSprite, zoneData.baseColor, zoneData.pointColor);
			figureThumbnail.Click += OnThumbnailClick;

			_figureThumbnailList.Add(figureThumbnail);
			
		}

		ShowThumbnailList();

		Color contentBgColor = zoneData.baseColor;
		contentBgColor.a = 0.5f;
		_contentBgImage.color = contentBgColor;

		_scrollHandleImage.color = zoneData.pointColor;

		//RectTransform rect = transform as RectTransform;
		//rect.sizeDelta = new Vector2(1080f, lineCount * 540f);
		//Vector2 contentContainerSize = _contentContainerRect.sizeDelta;
		//contentContainerSize.y = lineCount * 540f;
		//_contentContainerRect.sizeDelta = contentContainerSize;
	}

	private void ShowThumbnailList()
	{
		for(int i = 0; i < _figureThumbnailList.Count; i++)
		{
			ShowThumbnail(i);
		}
	}

	private void ShowThumbnail(int index)
	{
		//Debug.Log("index: " + index);
		DOVirtual.DelayedCall(index * 0.2f, _figureThumbnailList[index].Show);

		if(index == _figureThumbnailList.Count - 1)
		{
			Debug.Log("Finished.");
		}
	}





	private void OnPrepared()
	{
		if (!_figureService.isPrepared) return;
		if (!_zoneService.isPrepared) return;

		Init();
	}

	private void OnZoneChanged(string zoneId)
	{
		UpdateList(zoneId);
	}

	private void OnThumbnailClick(string figureId)
	{
		_figureService.ShowDetailPage(figureId);
		Debug.Log("figureId: " + figureId);
	}
}
