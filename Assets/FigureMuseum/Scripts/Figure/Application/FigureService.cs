﻿using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureService : MonoBehaviour 
{
	public delegate void FigureServiceEvent();
	public event FigureServiceEvent Prepared;
	public delegate void FigureDetailPageEvent(string figureId);
	public event FigureDetailPageEvent DetailPageShow;

	[SerializeField]
	private Figure _figure;
	[SerializeField]
	private FigureAssetBundleManager _figureAssetBundleManager;
	[SerializeField]
	private FigureThumbnailPool _thumbnailPool;
	[SerializeField]
	private ZoneService _zoneService;

	private bool _isPrepared = false;

	public bool isPrepared
	{
		get { return _isPrepared; }
	}





	private void Awake()
	{
		_figure.Prepared += OnPrepared;
		if (_figure.isPrepared) OnPrepared();

		_zoneService.Prepared += OnPrepared;
		if (_zoneService.isPrepared) OnPrepared();
	}

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	private void OnPrepared()
	{
		if (!_figure.isPrepared) return;
		if (!_zoneService.isPrepared) return;

		_isPrepared = true;
		if (Prepared != null) Prepared();
	}





	public List<GDEFigureData> GetFigureDataListByZoneId(string zoneId)
	{
		Debug.Log("zoneId: " + zoneId);
		GDEZoneData zoneData = _zoneService.GetZoneDataByZoneId(zoneId);

		List<GDEFigureData> figureDataList = new List<GDEFigureData>();
		for(int i = 0; i < zoneData.figureIdList.Count; i++)
		{
			string figureId = zoneData.figureIdList[i];
			figureDataList.Add(_figure.GetFiqureDataById(figureId));
		}
		return figureDataList;
	}

	public GDEFigureData GetFigureData(string figureId)
	{
		GDEFigureData figureData = _figure.GetFiqureDataById(figureId);
		return figureData;
	}

	public Sprite GetFigureThumbnail(string figureId)
	{
		return Resources.Load<Sprite>("FigureThumb/FigureThumb_" + figureId);
		//return _thumbnailPool.GetThumbnail(figureId);
	}

	public void ShowDetailPage(string figureId)
	{
		if (DetailPageShow != null) DetailPageShow(figureId);
	}

	public string GetFigureFilenameById(string figureId)
	{
		Debug.Log("figureId: " + figureId);
		return _figure.GetFigureFilenameById(figureId);
	}

	public AssetBundle GetFigureAsssetBundle()
	{
		return _figureAssetBundleManager.GetAssetBundle();
	}
}
