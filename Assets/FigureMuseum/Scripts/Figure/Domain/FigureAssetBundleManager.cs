﻿using DG.Tweening;
using Junhee.Io.AssetBundleLoader;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FigureAssetBundleManager : MonoBehaviour 
{
	[SerializeField]
	private ZoneService _zoneService;

	private AssetBundleLoader _assetBundleLoader;
	private AssetBundle _currentAssetBundle;
	private float _startTime;

	private void Awake()
	{
		_assetBundleLoader = GetComponent<AssetBundleLoader>();
	}

	private void Start ()
	{
		_assetBundleLoader.Completed += OnAssetLoadCompleted;
		_assetBundleLoader.Error += OnAssetLoadError;

		_zoneService.FloorChanged += OnFloorChanged;

		ChangeFigureAsset(5);
		//DOVirtual.DelayedCall(5f, ()=>ChangeFigureAsset(5));
	}
	
	private void Update ()
	{
	}

	private void OnDestroy()
	{
		_assetBundleLoader.Completed -= OnAssetLoadCompleted;
		_assetBundleLoader.Error -= OnAssetLoadError;

		_zoneService.FloorChanged -= OnFloorChanged;
	}





	private void OnFloorChanged(int floor)
	{
		ChangeFigureAsset(floor);
	}

	private void OnAssetLoadCompleted(AssetBundle assetBundle)
	{
		_currentAssetBundle = assetBundle;
		TraceBox.Log("figure asset load completed / name: " + _currentAssetBundle.name + " / time: " + (Time.fixedUnscaledTime - _startTime));
		Debug.Log("figure asset load completed / name: " + _currentAssetBundle.name + " / time: " + (Time.fixedUnscaledTime - _startTime));
	}

	private void OnAssetLoadError(AssetBundle assetBundle)
	{
		Debug.Log("figure asset load error");
	}





	private void ChangeFigureAsset(int floor)
	{
		_startTime = Time.fixedUnscaledTime;
		TraceBox.Log("load figure asset bundle");

		if (_currentAssetBundle != null)
		{
			Debug.Log("unload figure asset / name: " + _currentAssetBundle.name);
			_currentAssetBundle.Unload(true);
		}

		string filename;
		if (floor == 3) filename = "figure3f";
		else filename = "others";

#if UNITY_ANDROID && !UNITY_EDITOR
		DirectoryInfo directoryInfo = new DirectoryInfo(Application.persistentDataPath + "/AssetBundles/" + filename);
#else
		DirectoryInfo directoryInfo = new DirectoryInfo(Application.persistentDataPath + "/AssetBundles/" + filename);
#endif
		_assetBundleLoader.LoadFromFileAync(directoryInfo.FullName);
		//_assetBundleLoader.LoadByFile(directoryInfo.FullName);
	}





	public AssetBundle GetAssetBundle()
	{
		return _currentAssetBundle;
	}
}
