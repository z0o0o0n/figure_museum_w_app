﻿using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour 
{
	public delegate void FigureEvent();
	public event FigureEvent Prepared;

	private bool _isPrepared = false;
	private GDEFigureListData _figureListData;
	private GDEFigureFilenameMapData _figureFilenameMapData;

	public bool isPrepared
	{
		get { return _isPrepared; }
	}





	private void Awake()
	{
	}

	private void Start ()
	{
		_figureListData = new GDEFigureListData(GDEItemKeys.FigureList_FigureList);
		_figureFilenameMapData = new GDEFigureFilenameMapData(GDEItemKeys.FigureFilenameMap_FigureFilenameMap);
		_isPrepared = true;
		if (Prepared != null) Prepared();
	}





	public GDEFigureData GetFiqureDataById(string figureId)
	{
		for(int i = 0; i < _figureListData.figureList.Count; i++)
		{
			if(_figureListData.figureList[i].id == figureId)
			{
				return _figureListData.figureList[i];
			}
		}
		return null;
	}

	// filename : prefab file name of asset bundle
	public string GetFigureFilenameById(string figureId)
	{
		Debug.Log("figureId: " + figureId + " / filenameCount: " + _figureFilenameMapData.filename.Count);
		if (int.Parse(figureId) > _figureFilenameMapData.filename.Count - 1)
		{
			Debug.Log("figure filename list index out of range / figureId: " + figureId + " / filename list count: " + _figureFilenameMapData.filename.Count);
			return null;
		}
		else if (int.Parse(figureId) <= 0)
		{
			Debug.Log("figure filename list index can not be 0");
			return null;
		}

		string filename = _figureFilenameMapData.filename[int.Parse(figureId)];
		return filename;
	}
}
