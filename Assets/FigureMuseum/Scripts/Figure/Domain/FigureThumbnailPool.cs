﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureThumbnailPool : MonoBehaviour 
{
	[SerializeField]
	private List<Sprite> _thumbnailList;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	public Sprite GetThumbnail(string figureId)
	{
		int index = int.Parse(figureId);
		return _thumbnailList[index];
	}
}
