﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FBXLoader : MonoBehaviour 
{
	private string _modelPath = "SuperGirl"; //"Deadpool/Deadpool"

	private void Start ()
	{
		//LoadMeshes();
	}
	
	private void Update ()
	{
		//if(Input.GetKeyUp(KeyCode.A))
		//{
		//	LoadFbx();
		//}
	}





	private void LoadMeshes()
	{
		Mesh[] meshes = Resources.LoadAll<Mesh>(_modelPath);
		Debug.Log("FBXLoader start");
		foreach (Mesh m in meshes)
		{
			Debug.Log("meshname: " + m.name);
		}
	}

	private void LoadFbx()
	{
		float startTime = Time.realtimeSinceStartup;
		GameObject instance = Instantiate(Resources.Load(_modelPath)) as GameObject;
		Debug.Log("loading time: " + (Time.realtimeSinceStartup - startTime));
		instance.transform.parent = transform;
	}
}
