﻿using DG.Tweening;
using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoneBanner : MonoBehaviour 
{
	[SerializeField]
	private ZoneService _zoneServcie;
	//[SerializeField]
	//private List<Sprite> _bannerSprites;
	[SerializeField]
	private Image _bannerImage;
	[SerializeField]
	private Image _bgImage;
	[SerializeField]
	private Text _titleText;
	[SerializeField]
	private Text _descText;
	[SerializeField]
	private Image _zoneTitleBg;
	[SerializeField]
	private Image _line;
	private int _currentFloor = 0;

	private void Awake()
	{
		_zoneServcie.Prepared += OnPrepared;
		_zoneServcie.ZoneChanged += OnZoneChanged;
		_zoneServcie.FloorChanged += OnFloorChanged;
		if (_zoneServcie.isPrepared) OnPrepared();
	}

	private void Start ()
	{
		
	}
	
	private int _zoneIndexTemp = 0;
	private void Update ()
	{
		//if(Input.GetMouseButtonUp(0))
		//{
		//	_zoneIndexTemp++;
		//	if (_zoneIndexTemp >= 2) _zoneIndexTemp = 0;
		//	_zoneServcie.ChangZoneTemp(_zoneIndexTemp);
		//}
	}

	private void OnDestroy()
	{
		_zoneServcie.Prepared -= OnPrepared;
		_zoneServcie.ZoneChanged -= OnZoneChanged;
		_zoneServcie.FloorChanged -= OnFloorChanged;
	}





	private void OnPrepared()
	{

	}

	private void OnFloorChanged(int floor)
	{
		_currentFloor = floor;
	}

	private void OnZoneChanged(string zoneId)
	{
		ChangeBanner(zoneId);

		//if(zoneId == "0")
		//{
		//	_titleText.gameObject.SetActive(false);
		//	_zoneTitleBg.gameObject.SetActive(false);
		//	_zoneTitleBg.rectTransform.DOSizeDelta(new Vector2(0f, 64f), 0f);

		//}
		//else
		//{
		//	_titleText.gameObject.SetActive(true);
		//	_zoneTitleBg.gameObject.SetActive(true);
		//}
	}





	private void ChangeBanner(string zoneId)
	{
		GDEZoneData zoneData = _zoneServcie.GetZoneDataByZoneId(zoneId);

		float time = 0.2f;
		_bannerImage.DOFade(0f, time).SetEase(Ease.OutSine).OnComplete(() => OnBannerHided(zoneId, zoneData));
		_titleText.DOFade(0f, time).SetEase(Ease.OutSine);
		_descText.DOFade(0f, time).SetEase(Ease.OutSine);

		_zoneTitleBg.rectTransform.DOSizeDelta(new Vector2(0f, 64f), time).SetEase(Ease.OutSine);
	}

	private void OnBannerHided(string zoneId, GDEZoneData zoneData) // 교체 과정에서 기존 배너가 사라진 상태
	{
		// 초기화
		_bannerImage.rectTransform.DOScale(1.1f, 0f);
		//_titleText.transform.DOLocalMoveX(-492f + 30f, 0f).SetId("titleText.MoveX." + _titleText.GetInstanceID());
		_descText.transform.DOLocalMoveX(-492f + 30f, 0f).SetId("descText.MoveX." + _descText.GetInstanceID());

		// 교체
		float time = 0.3f;
		int zoneIndex = int.Parse(zoneId);
		//_bannerImage.sprite = _bannerSprites[zoneIndex];
		_bannerImage.sprite = Resources.Load<Sprite>("ZoneBanner/ZoneBannerImage_" + _currentFloor);
		_bannerImage.DOFade(1f, time).SetEase(Ease.InSine);
		_bannerImage.rectTransform.DOScale(1f, 0.5f).SetEase(Ease.OutCubic);

		_titleText.DOFade(1f, 0f).SetEase(Ease.InSine);
		//_titleText.transform.DOLocalMoveX(-492f, time).SetDelay(0.2f).SetId("titleText.MoveX." + _titleText.GetInstanceID()).SetEase(Ease.OutCubic);
		_titleText.text = zoneData.name;
		_titleText.color = zoneData.baseColor;
		//Debug.Log("x: " + _titleText.preferredWidth);

		DOTween.Kill("descText.MoveX." + _descText.GetInstanceID());
		_descText.rectTransform.DOLocalMoveX(_titleText.preferredWidth + 320f - (1440 / 2), 0f).SetId("descText.MoveX." + _descText.GetInstanceID());
		_descText.DOFade(1f, time).SetDelay(0.3f).SetEase(Ease.InSine);
		//_descText.transform.DOLocalMoveX(-492f, time).SetDelay(0.3f).SetId("descText.MoveX." + _descText.GetInstanceID()).SetEase(Ease.OutCubic);

		_bgImage.DOColor(zoneData.baseColor, time);

		_zoneTitleBg.rectTransform.DOSizeDelta(new Vector2(_titleText.preferredWidth + 140f, 64f), time).SetEase(Ease.OutCubic);
		_zoneTitleBg.DOColor(zoneData.pointColor, time);
		_line.DOColor(zoneData.pointColor, time);
	}
}
