﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationDisplay : MonoBehaviour 
{
	[SerializeField]
	private Text _text;
	[SerializeField]
	private ZoneService _zoneService;

	private void Awake()
	{
		_zoneService.ZoneChanged += OnZoneChanged;
		_zoneService.FloorChanged += OnFloorChanged;
	}

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_zoneService.ZoneChanged -= OnZoneChanged;
		_zoneService.FloorChanged -= OnFloorChanged;
	}





	private void OnZoneChanged(string zoneId)
	{
		if(zoneId == "0")
		{
			_text.gameObject.SetActive(false);
		}
		else
		{
			_text.gameObject.SetActive(true);
		}
	}

	private void OnFloorChanged(int floor)
	{
		string floorName = _zoneService.GetFloorName(floor);
		if (string.IsNullOrEmpty(floorName)) return;

		_text.text = floorName;
	}
}
