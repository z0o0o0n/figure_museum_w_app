﻿using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneService : MonoBehaviour 
{
	public delegate void ZoneServiceEvent();
	public delegate void ZoneChangeEvent(string zoneId);
	public delegate void FloorChangeEvent(int floor);
	public event ZoneServiceEvent Prepared;
	public event ZoneChangeEvent ZoneChanged;
	public event FloorChangeEvent FloorChanged;
	public event ZoneServiceEvent ZoneEmpty;

	[SerializeField]
	private Zone _zone;
	[SerializeField]
	private BeaconService _beaconService;

	private bool _isPrepared = false;

	public bool isPrepared
	{
		get { return _isPrepared; }
	}





	private void Awake()
	{
		_zone.Prepared += OnPrepared;
		_zone.ZoneChanged += OnZoneChanged;
		_zone.FloorChanged += OnFloorChanged;
		if (_zone.isPrepared) OnPrepared();

		_beaconService.Empty += OnBeaconEmpty;
		_beaconService.Changed += OnBeaconChanged;
	}

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_zone.Prepared -= OnPrepared;
		_zone.ZoneChanged -= OnZoneChanged;
		_zone.FloorChanged -= OnFloorChanged;

		_beaconService.Empty -= OnBeaconEmpty;
		_beaconService.Changed -= OnBeaconChanged;
	}





	private void OnPrepared()
	{
		_isPrepared = true;
		if (Prepared != null) Prepared();
	}

	private void OnZoneChanged(string zoneId)
	{
		if (ZoneChanged != null) ZoneChanged(zoneId);
	}

	private void OnFloorChanged(int floor)
	{
		if (FloorChanged != null) FloorChanged(floor);
	}

	private void OnBeaconEmpty(BeaconSettingData beaconSettingData)
	{
	}

	private void OnBeaconChanged(BeaconSettingData beaconSettingData)
	{
		_zone.ChangeZone(beaconSettingData.zoneId);
	}





	public GDEZoneData GetZoneDataByZoneId(string zoneId)
	{
		return _zone.GetZoneData(zoneId);
	}

	public GDEZoneData GetZoneDataByFigureId(string figureId)
	{
		return _zone.GetZoneDataByFigureId(figureId);
	}

	public void NextZone()
	{
		_zone.NextZone();
	}

	public void ChangZoneTemp(string zoneId)
	{
		_zone.ChangeZone(zoneId);
	}

	public string GetFloorName(int floor)
	{
		return _zone.GetFloorName(floor);
	}

	public int GetCurrentFloor()
	{
		return _zone.GetCurrentFloor();
	}
}
