﻿using GameDataEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour 
{
	public delegate void ZoneEvent();
	public delegate void ZoneChangeEvent(string zoneId);
	public delegate void FloorChangeEvent(int floor);
	public event ZoneEvent Prepared;
	public event ZoneChangeEvent ZoneChanged;
	public event FloorChangeEvent FloorChanged;

	private bool _isPrepared = false;
	private string _currentZoneId = "-1";
	private int _currentFloor = 0;
	private GDEZoneListData _zoneListData;
	private GDEFloorNameListData _floorNameListData;
	private List<string> _testZoneIdList = new List<string>{ "10", "25", "20", "22", "23", "24", "38", "30", "31", "32", "33", "34", "37", "35", "36", "42", "40", "44", "50", "51" };

	public bool isPrepared
	{
		get { return _isPrepared; }
	}





	private void Awake()
	{
		
	}

	private void Start ()
	{
		_zoneListData = new GDEZoneListData(GDEItemKeys.ZoneList_ZoneList);
		_floorNameListData = new GDEFloorNameListData(GDEItemKeys.FloorNameList_FloorNameList);
		_isPrepared = true;
		if (Prepared != null) Prepared();
	}

	private void Update ()
	{
		//if(Input.GetKeyUp(KeyCode.A))
		//{
		//	NextZone();
		//}
	}

	private int _tempZoneIndex = -1;
	public void NextZone()
	{
		_tempZoneIndex++;
		if (_tempZoneIndex >= _testZoneIdList.Count) _tempZoneIndex = 0;

		ChangeZone(_testZoneIdList[_tempZoneIndex]);
	}





	public void ChangeZone(string zoneId)
	{
		// zone 변경
		if (_currentZoneId == zoneId) return;

		GDEZoneData zoneData = GetZoneData(zoneId);
		if (zoneData == null) return;

		_currentZoneId = zoneData.id;

		if (ZoneChanged != null) ZoneChanged(zoneData.id);

		// floor 변경 확인
		if (zoneData.floor == _currentFloor) return;

		_currentFloor = zoneData.floor;
	
		if (FloorChanged != null) FloorChanged(zoneData.floor);
	}

	public GDEZoneData GetZoneDataByFigureId(string figureId)
	{
		int zoneDataCount = _zoneListData.zoneList.Count;
		for (int i = 0; i < zoneDataCount; i++)
		{
			for (int j = 0; j < _zoneListData.zoneList[i].figureIdList.Count; j++)
			{
				if(_zoneListData.zoneList[i].figureIdList[j] == figureId)
				{
					return _zoneListData.zoneList[i];
				}
			}
		}
		return null;
	}

	public GDEZoneData GetZoneData(string zoneId)
	{
		int zoneDataCount = _zoneListData.zoneList.Count;
		for(int i = 0; i < zoneDataCount; i++)
		{
			if(_zoneListData.zoneList[i].id == zoneId)
			{
				return _zoneListData.zoneList[i];
			}
		}
		return null;
	}

	public string GetFloorName(int floor)
	{
		string result = null;
		if (floor == -2) result = _floorNameListData.b2name;
		else if (floor == -1) result = _floorNameListData.b1name;
		else if (floor == 1) result = _floorNameListData.f1name;
		else if (floor == 2) result = _floorNameListData.f2name;
		else if (floor == 3) result = _floorNameListData.f3name;
		else if (floor == 4) result = _floorNameListData.f4name;
		else if (floor == 5) result = _floorNameListData.f5name;
		return result;
	}

	public int GetCurrentFloor()
	{
		return _currentFloor;
	}
}
