﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour 
{
	//public void d

	[SerializeField]
	private GameObject _bg;
	//[SerializeField]
	//private Image _progressLine;
	[SerializeField]
	private Text _text;

	private float _duration = 0f;

	private void Start ()
	{
		_text.DOFade(1f, 0.1f).SetLoops(-1, LoopType.Yoyo).SetId("text.fade." + _text.GetInstanceID());
	}
	
	private void Update ()
	{
		
	}





	private void OnOpened()
	{
		DOTween.Kill("bg.moveX." + _bg.GetInstanceID());
		_bg.transform.DOLocalMoveX(-1140f, _duration).SetId("bg.moveX." + _bg.GetInstanceID()).SetEase(Ease.InQuint);

		PlayLoadingMotion();
	}

	private void OnClosed()
	{
		DOTween.Kill("bg.moveX." + _bg.GetInstanceID());
		_bg.transform.DOLocalMoveX(1140f, _duration).SetId("bg.moveX." + _bg.GetInstanceID()).SetEase(Ease.InQuint);
	}





	private void PlayLoadingMotion()
	{
		//DOTween.Kill("progressLine.scale." + _progressLine.GetInstanceID());	
		//_progressLine.transform.DOScaleX(1f, 0.5f).SetId("progressLine.scale." + _progressLine.GetInstanceID());
	}





	public void SetColor(Color bgColor, Color pointColor)
	{
		_bg.GetComponent<Image>().color = bgColor;
	}

	public void Open(float duration)
	{
		_duration = duration;

		_bg.transform.localPosition = new Vector3(1140f, 0f, 0f);

		DOTween.Kill("bg.moveX." + _bg.GetInstanceID());
		_bg.transform.DOLocalMoveX(0f, duration).SetId("bg.moveX." + _bg.GetInstanceID()).SetEase(Ease.OutQuint).OnComplete(OnOpened);
	}

	public void Close(float duration)
	{
		_duration = duration;

		_bg.transform.localPosition = new Vector3(-1140f, 0f, 0f);

		DOTween.Kill("bg.moveX." + _bg.GetInstanceID());
		_bg.transform.DOLocalMoveX(0f, duration).SetId("bg.moveX." + _bg.GetInstanceID()).SetEase(Ease.OutQuint).OnComplete(OnClosed);
	}

	public void Reset()
	{
		DOTween.Kill("bg.moveX." + _bg.GetInstanceID());
		_bg.transform.localPosition = new Vector3(1140f, 0f, 0f);
	}
}
