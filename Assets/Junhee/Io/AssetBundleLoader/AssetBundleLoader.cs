﻿namespace Junhee.Io.AssetBundleLoader
{
	using UnityEngine;
	using UnityEngine.UI;
	using System.Collections;
	using System;
	using System.IO;
	using Utils;

	public class AssetBundleLoader : MonoBehaviour
	{
		public delegate void AssetBundleLoadEvent(AssetBundle assetBundle);
		public event AssetBundleLoadEvent Completed;
		public event AssetBundleLoadEvent Error;

		private void Start()
		{
		}

		private void Update()
		{
			// example
			//if (Input.GetKeyUp(KeyCode.A))
			//{
			//	DirectoryInfo directory = new DirectoryInfo(Application.persistentDataPath + "/AssetBundles/character");
			//	Load("file://" + directory.FullName);
			//}
		}





		public void Load(string path)
		{
			StartCoroutine(loadAssetBundle(path));
		}

		private IEnumerator loadAssetBundle(string path)
		{
			WWW www = WWW.LoadFromCacheOrDownload(path, 0);
			yield return www;

			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.Log("asset bundle load error: " + www.error);
				if (Error != null) Error(null);
			}

			Debug.Log("asset load completed");
			if (Completed != null) Completed(www.assetBundle);
		}

		public void LoadByFile(string path)
		{
			AssetBundle assetbundle = AssetBundle.LoadFromFile(path);
			Debug.Log("asset load completed");
			if (Completed != null) Completed(assetbundle);
		}

		public void LoadFromFileAync(string path)
		{
			StartCoroutine(LoadFromFileAyncCoroutine(path));
		}

		private IEnumerator LoadFromFileAyncCoroutine(string path)
		{
			AssetBundleCreateRequest assetbundleCreateRequest = AssetBundle.LoadFromFileAsync(path);
			yield return assetbundleCreateRequest;

			AssetBundle assetBundle = assetbundleCreateRequest.assetBundle;
			if(assetBundle == null)
			{
				Debug.Log("asset bundle load error");
				if (Error != null) Error(null);
			}

			Debug.Log("asset load completed");
			if (Completed != null) Completed(assetBundle);
		}

		//private IEnumerator LoadPrefeb(AssetBundle assetBundle)
		//{
		//	AssetBundleRequest prefebAsset = assetBundle.LoadAssetAsync<GameObject>("Character_1");
		//	yield return prefebAsset;

		//	GameObject prefeb = prefebAsset.asset as GameObject;
		//	GameObject instance = Instantiate(prefeb);
		//	instance.transform.SetParent(transform);
		//	instance.transform.localScale = Vector3.one;
		//	instance.transform.localPosition = Vector3.zero;
		//}
	}
}