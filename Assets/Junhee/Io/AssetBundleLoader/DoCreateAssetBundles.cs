﻿#if UNITY_EDITOR
using UnityEditor;

public class DoCreateAssetBundles
{
	[MenuItem("Assets/Build Asset Bundles")]

	static void CreateBundles()
	{
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.Android);
	}
}
#endif