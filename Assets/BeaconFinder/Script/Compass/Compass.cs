﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour 
{
	[SerializeField]
	private Text _compasDisplayText;

	private void Start ()
	{
		Input.compass.enabled = true;
		Input.location.Start();
	}
	
	private void Update ()
	{
		string log = "";
		log += "magneticHeading: " + Input.compass.magneticHeading.ToString();
		log += " / trueHeading: " + Input.compass.trueHeading.ToString() + "\n";
		log += "headingAccuracy: " + Input.compass.headingAccuracy.ToString();
		log += " / rawVector: " + Input.compass.rawVector.ToString() + "\n";
		log += "acc: " + Input.acceleration.ToString();
		_compasDisplayText.text = log;
		//Debug.Log("방향 정확도: " + Input.compass.headingAccuracy + " / 각도단위 방향: " + Input.compass.magneticHeading);
	}
}
