﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingService : MonoBehaviour 
{
	public delegate void SettingServiceEvent();
	public event SettingServiceEvent Updated;
	public event SettingServiceEvent UpdateFailed;

	[SerializeField]
	private Setting _setting;
	private bool _isPrepared = false;

	public bool isPrepared
	{
		get { return _isPrepared; }
	}





	private void Awake()
	{
		_setting.Prepared += OnPrepared;
		_setting.Updated += OnSettingFileUpdated;
		_setting.UpdateFailed += OnSettingFileUpdateFailed;

		if (_setting.isPrepared) OnPrepared();
	}

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_setting.Prepared -= OnPrepared;
		_setting.Updated -= OnSettingFileUpdated;
		_setting.UpdateFailed -= OnSettingFileUpdateFailed;
	}





	private void OnPrepared()
	{
		if (!_setting.isPrepared) return;

		_isPrepared = true;
	}

	private void OnSettingFileUpdated()
	{
		if (Updated != null) Updated();
	}

	private void OnSettingFileUpdateFailed()
	{
		if (UpdateFailed != null) UpdateFailed();
	}





	public void UpdateSettingFile()
	{
		_setting.UpdateSettingFile();
	}

	public BeaconSettingData GetBeaconSettingDataByNamespaceId(string namespaceId)
	{
		SettingData settingData = _setting.GetSettingData();

		BeaconSettingData beaconSettingData = null;
		for (int i = 0; i < settingData.beaconSettingData.Count; i++)
		{
			if (settingData.beaconSettingData[i].ns == namespaceId)
			{
				beaconSettingData = settingData.beaconSettingData[i];
			}
		}
		return beaconSettingData;
	}

	public BeaconSettingData GetBeaconSettingData(string beaconId)
	{
		SettingData settingData = _setting.GetSettingData();

		BeaconSettingData beaconSettingData = null;
		for (int i = 0; i < settingData.beaconSettingData.Count; i++)
		{
			if (settingData.beaconSettingData[i].beaconId == beaconId)
			{
				beaconSettingData = settingData.beaconSettingData[i];
			}
		}
		return beaconSettingData;
	}

	public string GetBeaconIdByNamespaceId(string namespaceId)
	{
		SettingData settingData = _setting.GetSettingData();

		string beaconId = null;
		for(int i = 0; i < settingData.beaconSettingData.Count; i++)
		{
			//Debug.Log("ns: " + settingData.beaconSettingData[i].ns + " / " + namespaceId);
			if(settingData.beaconSettingData[i].ns == namespaceId)
			{
				beaconId = settingData.beaconSettingData[i].beaconId;
			}
		}
		return beaconId;
	}

	public string GetZoneIdByBeaconId(string beaconId)
	{
		SettingData settingData = _setting.GetSettingData();

		string zoneId = null;
		for (int i = 0; i < settingData.beaconSettingData.Count; i++)
		{
			if (settingData.beaconSettingData[i].beaconId == beaconId)
			{
				zoneId = settingData.beaconSettingData[i].zoneId;
			}
		}
		return zoneId;
	}

	public int GetDirectionByBeaconId(string beaconId)
	{
		SettingData settingData = _setting.GetSettingData();

		int direction = -1;
		for (int i = 0; i < settingData.beaconSettingData.Count; i++)
		{
			//Debug.Log("beadonId: " + settingData.beaconSettingData[i].beaconId + " / " + beaconId);
			if (settingData.beaconSettingData[i].beaconId == beaconId)
			{
				direction = settingData.beaconSettingData[i].direction;
			}
		}

		return direction;
	}

	public SettingData GetSettingData()
	{
		return _setting.GetSettingData();
	}
}
