﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingUpdateButton : MonoBehaviour 
{
	[SerializeField]
	private SettingService _settingService;
	private Button _button;

	private void Awake()
	{
		_button = GetComponent<Button>();

		_settingService.Updated += OnSettingUpdated;
		_settingService.UpdateFailed += OnSettingUpdateFailed;
	}

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_settingService.Updated -= OnSettingUpdated;
		_settingService.UpdateFailed -= OnSettingUpdateFailed;
	}





	public void OnButtonClick()
	{
		gameObject.SetActive(false);
		_settingService.UpdateSettingFile();
	}

	private void OnSettingUpdated()
	{
		gameObject.SetActive(true);
	}

	private void OnSettingUpdateFailed()
	{
		gameObject.SetActive(true);
	}
}
