﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconSettingData 
{
	private string _beaconId;
	private string _ns;
	private string _zoneId;
	private int _direction;

	public BeaconSettingData(string beaconId, string ns, string zoneId, int direction)
	{
		_beaconId = beaconId;
		_ns = ns;
		_zoneId = zoneId;
		_direction = direction;
	}

	public string beaconId
	{
		get { return _beaconId; }
	}

	public string ns
	{
		get { return _ns; }
	}

	public string zoneId
	{
		get { return _zoneId; }
	}

	public int direction
	{
		get { return _direction; }
	}
}
