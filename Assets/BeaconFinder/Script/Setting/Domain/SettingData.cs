﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingData
{
	public string settingFileVersion;
	public int beaconCount;
	public int compasRange;
	public int minRssi;
	public List<BeaconSettingData> beaconSettingData;
}
