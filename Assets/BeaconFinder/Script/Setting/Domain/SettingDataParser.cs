﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingDataParser : MonoBehaviour 
{
	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	private string ToStringArray(string[] stringArray)
	{
		string log = "";
		for (int i = 0; i < stringArray.Length; i++)
		{
			if(i == 0)
			{
				log += stringArray[i];
			}
			else
			{
				log += ", " + stringArray[i];
			}
		}
		return log;
	}





	public SettingData Parse(string rawData)
	{
		int variableCount = 4;
		Dictionary<string, string> variables = new Dictionary<string, string>();
		string[] rows = rawData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

		// get variables
		for (int i = 0; i < variableCount; i++)
		{
			if(rows[i] != "")
			{
				string[] rowFields = rows[i].Split(',');
				variables.Add(rowFields[0], rowFields[1]);
				Debug.Log("variable name: " + rowFields[0] + " / value: " + rowFields[1]);
			}
		}

		SettingData settingData = new SettingData();
		settingData.settingFileVersion = variables["settingFileVersion"];
		settingData.beaconCount = int.Parse(variables["beaconCount"]);
		settingData.compasRange = int.Parse(variables["compasRange"]);
		settingData.minRssi = int.Parse(variables["minRssi"]);

		// get beacon setting data
		settingData.beaconSettingData = new List<BeaconSettingData>();
		for (int i = variableCount; i < settingData.beaconCount + variableCount; i++)
		{
			string[] rowFields = rows[i].Split(',');
			BeaconSettingData beaconSettingData = new BeaconSettingData(rowFields[0], "0X" + rowFields[1], rowFields[2], int.Parse(rowFields[3]));
			settingData.beaconSettingData.Add(beaconSettingData);
			Debug.Log("beacon setting data / beaconId: " + rowFields[0] + " / ns: " + rowFields[1] + " / zoneId: " + rowFields[2] + " / direction: " + rowFields[3]);
		}

		return settingData;
	}
}
