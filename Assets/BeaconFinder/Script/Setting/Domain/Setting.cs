﻿using MOD.Net.Http;
using System.IO;
using UnityEngine;

public class Setting : MonoBehaviour
{
	public delegate void SettingDataManagerEvent();
	public event SettingDataManagerEvent Prepared;
	public event SettingDataManagerEvent Updated;
	public event SettingDataManagerEvent UpdateFailed;

	[SerializeField]
	private HttpRequest _httpRequest;
	[SerializeField]
	private SettingDataParser _settingDataParser;
	private bool _isPrepared = false;
	private bool _isUpdating = false;
	private SettingData _settingData;
	private string _settingFilePath;

	public bool isPrepared
	{
		get { return _isPrepared; }
	}





	private void Awake()
	{
		DirectoryInfo folder;
#if UNITY_EDITOR
		folder = new DirectoryInfo(Application.persistentDataPath);
		_settingFilePath = folder.FullName + "/Setting.csv";
#elif UNITY_ANDROID
		folder = new DirectoryInfo(Application.persistentDataPath + "/..");
		_settingFilePath = folder.FullName + "/Setting.csv";
#endif
		Debug.Log("setting file path: " + _settingFilePath);

		LoadLocalSettingData();
	}

	private void Start()
	{

	}

	private void Update()
	{

	}

	private void OnDestroy()
	{

	}





	private void LoadLocalSettingData()
	{
		if (!File.Exists(_settingFilePath))
		{
			Debug.Log("setting file이 없습니다.");
			UpdateSettingFile();
			return;
		}

		string settingData = File.ReadAllText(_settingFilePath);
		_settingData = _settingDataParser.Parse(settingData);

		_isPrepared = true;
		if (Prepared != null) Prepared();

		Debug.Log("현재 setting file 버전: " + _settingData.settingFileVersion);
	}

	private void SaveToLocal(string wwwText)
	{
		Debug.Log("setting file path: " + _settingFilePath);
		File.WriteAllText(_settingFilePath, wwwText);
		Debug.Log("saved setting file to local");
	}





	private void OnUpdateCompleted(string id, string wwwText)
	{
		_isUpdating = false;

		_httpRequest.Complete -= OnUpdateCompleted;
		_httpRequest.TimeOut -= OnUpdateError;
		_httpRequest.Error -= OnUpdateError;

		SaveToLocal(wwwText);

		_settingData = _settingDataParser.Parse(wwwText);

		_isPrepared = true;
		if (Prepared != null) Prepared();
		if (Updated != null) Updated();

		Debug.Log("updated setting file. ver: " + _settingData.settingFileVersion);
	}

	private void OnUpdateError(string id, string wwwText)
	{
		_httpRequest.Complete -= OnUpdateCompleted;
		_httpRequest.TimeOut -= OnUpdateError;
		_httpRequest.Error -= OnUpdateError;

		Debug.Log("Error - setting file update failed");

		if (UpdateFailed != null) UpdateFailed();
	}





	public void UpdateSettingFile()
	{
		Debug.Log("UpdateSettingFile");
		if (!_isUpdating)
		{
			_isPrepared = false;
			_isUpdating = true;

			_httpRequest.Complete += OnUpdateCompleted;
			_httpRequest.TimeOut += OnUpdateError;
			_httpRequest.Error += OnUpdateError;
			_httpRequest.Get("SettingFileUpdate", "https://storage.googleapis.com/com-thumb-test.appspot.com/Setting.csv");
			Debug.Log("setting file을 update 합니다.");
		}
	}

	public SettingData GetSettingData()
	{
		return _settingData;
	}
}