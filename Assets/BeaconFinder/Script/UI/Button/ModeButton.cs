﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeButton : MonoBehaviour 
{
	public delegate void ModeButtonEvent(bool mode);
	public event ModeButtonEvent ModeChanged;

    private bool _currentMode = false;

	void Start () 
    {
		
	}
	
	void Update () 
    {
		
	}





    public void OnClick()
    {
		if(_currentMode)
		{
			_currentMode = false;
		}
		else if(!_currentMode)
		{
			_currentMode = true;
		}

		if (ModeChanged != null) ModeChanged(_currentMode);
	}





	public bool GetCurrentMode()
	{
		return _currentMode;
	}

	public void SetCurrentMode(bool mode)
	{
		_currentMode = mode;
		if (ModeChanged != null) ModeChanged(_currentMode);
	}
}
