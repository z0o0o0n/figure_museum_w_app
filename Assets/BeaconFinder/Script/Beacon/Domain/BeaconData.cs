﻿namespace com.mod.fm.beacon
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class BeaconData
	{
		public string id;
		public string namespaceId;
		public List<int> rssiList;
		public int rssi;
		public int normalRssi;
		public string[] zoneIds;
	}

}