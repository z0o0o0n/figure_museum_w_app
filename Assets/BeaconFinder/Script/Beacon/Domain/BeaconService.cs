﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

internal class BeaconService : MonoBehaviour
{
	public delegate void BeaconServiceEvent();
	public delegate void BeaconChangeEvent(BeaconSettingData beaconSettingData);
	public event BeaconServiceEvent ScanStarted;
	public event BeaconServiceEvent ScanStoped;
	public event BeaconChangeEvent Changed;
	public event BeaconChangeEvent Empty;

	public enum BroadcastState { ACTIVATE, DEACTIVATE };

	[SerializeField]
	private SettingService _settingService;
	[SerializeField]
	private Text _rawBeaconLog;
	[SerializeField]
	private Text _filteredBeaconLog;
	[SerializeField]
	private Text _selectedBeaconLog;
	[SerializeField]
	private com.mod.fm.beacon.Beacon _beacon;

	private string _regionName = "com.mod.fm";
	//private int _compasRange = 100;
	private BeaconType _beaconType = BeaconType.EddystoneUID;
	private BroadcastState _broadcastState = BroadcastState.DEACTIVATE;
	private List<Beacon> mybeacons = new List<Beacon>();

	public bool isActivated
	{
		get
		{
			if (_broadcastState == BroadcastState.ACTIVATE) return true;
			else return false;
		}
	}





	private void Awake()
	{
		_settingService.Updated += OnSettingFileUpdated;
	}

	private void Start()
	{
		BluetoothState.BluetoothStateChangedEvent += OnBluetoothStateChanged;
		BluetoothState.Init();
	}

	private void OnDestroy()
	{
		_settingService.Updated -= OnSettingFileUpdated;
	}

	private void OnApplicationPause(bool pause)
	{
		if (pause)
		{
			Debug.Log("App Pause");
			StopScan();
		}
		else
		{
			Debug.Log("App Restart");
			if (_settingService.isPrepared) StartScan();
		}
	}




	private Dictionary<string, int> _tempBeaconRssiCount = new Dictionary<string, int>();
	private void DisplayOnBeaconFound()
	{
		Dictionary<string, List<int>> tempBeaconRssis = new Dictionary<string, List<int>>();
		_tempBeaconRssiCount = new Dictionary<string, int>();
		string beaconId;
		string zoneId;
		BeaconSettingData beaconSettingData;

		foreach (Beacon b in mybeacons)
		{
			if (b.type == BeaconType.EddystoneUID)
			{
				Debug.Log("fond Beacon: " + b.ToString());
				string uuid = b.UUID.ToUpper();
				if (tempBeaconRssis.ContainsKey(uuid))
				{
					tempBeaconRssis[uuid].Add(b.rssi);
				}
				else
				{
					tempBeaconRssis.Add(uuid, new List<int>());
					tempBeaconRssis[uuid].Add(b.rssi);
				}
				//_beacon.AddBeaconInfo(uuid, b.rssi);
				//Debug.Log("Beacon Id: " + _settingService.GetBeaconIdByNamespaceId(uuid));
			}
		}

		string rawBeaconLog = "";
		Dictionary<string, int> tempBeaconNormalRssi = new Dictionary<string, int>();
		foreach (KeyValuePair<string, List<int>> items in tempBeaconRssis)
		{
			string rssiListString = "";
			int totalRssi = 0;
			for (int i = 0; i < items.Value.Count; i++)
			{
				totalRssi += items.Value[i];
				rssiListString += " / " + items.Value[i];
			}

			//int lastRssi = items.Value[items.Value.Count - 1];
			int normalRssi = totalRssi / items.Value.Count;
			tempBeaconNormalRssi.Add(items.Key, normalRssi);
			_tempBeaconRssiCount.Add(items.Key, items.Value.Count);

			beaconSettingData = _settingService.GetBeaconSettingDataByNamespaceId(items.Key);

			if (beaconSettingData != null)
			{
				beaconId = beaconSettingData.beaconId;
				zoneId = beaconSettingData.zoneId;
				float direction = beaconSettingData.direction;
				//string namespaceIdNum = items.Key.Substring(19, 3);
				rawBeaconLog += "zone: " + zoneId + " / beacon: " + beaconId + " / n_rssi: " + normalRssi + " / c: " + items.Value.Count + " / d: " + direction + "\n";
			}
		}
		_rawBeaconLog.text = rawBeaconLog;

		if (tempBeaconNormalRssi.Count <= 0)
		{
			return;
		}

		// 수신 강도가 강한 순서대로 정렬
		List<KeyValuePair<string, int>> list = tempBeaconNormalRssi.ToList();
		list.Sort(
			delegate (KeyValuePair<string, int> pair1, KeyValuePair<string, int> pair2)
			{
				return pair2.Value.CompareTo(pair1.Value);
			}
		);

		List<string> filteredBeaconIds = FilterDirectionOut(list);

		if (filteredBeaconIds.Count <= 0)
		{
			_selectedBeaconLog.text = "null";
			if (Empty != null) Empty(null);
			return;
		}

		beaconId = filteredBeaconIds[0];
		beaconSettingData = _settingService.GetBeaconSettingData(beaconId);

		_selectedBeaconLog.text = beaconSettingData.zoneId;

		if (Changed != null) Changed(beaconSettingData);
	}

	private List<string> FilterDirectionOut(List<KeyValuePair<string, int>> list)
	{
		// 방향이 유사한 것만 필터링
		List<string> filteredBeaconIds = new List<string>();
		string filteredBeaconLog = "";
		for (int i = 0; i < list.Count; i++)
		{
			BeaconSettingData beaconSettingData = _settingService.GetBeaconSettingDataByNamespaceId(list[i].Key);

			if (beaconSettingData != null)
			{
				string beaconId = beaconSettingData.beaconId;
				string zoneId = beaconSettingData.zoneId;
				string namespaceId = beaconSettingData.ns;
				float direction = beaconSettingData.direction;

				// 방향이 -1이거나 rssi가 minRssi보다 작다면 걸러냄

				if(direction == -2)
				{
					filteredBeaconIds.Add(beaconId);
					filteredBeaconLog += "zone: " + zoneId + " / beacon: " + beaconId + " / n_rssi: " + list[i].Value + " / c: " + _tempBeaconRssiCount[list[i].Key] + " / d: " + direction + "\n";
				}
				else if (direction >= 0 && list[i].Value > _settingService.GetSettingData().minRssi)
				{
					if (IsSameDirection(direction, _settingService.GetSettingData().compasRange))
					{
						filteredBeaconIds.Add(beaconId);
						filteredBeaconLog += "zone: " + zoneId + " / beacon: " + beaconId + " / n_rssi: " + list[i].Value + " / c: " + _tempBeaconRssiCount[list[i].Key] + " / d: " + direction + "\n";
					}
				}
			}
		}
		_filteredBeaconLog.text = filteredBeaconLog;
		return filteredBeaconIds;
	}

	private bool IsSameDirection(float direction, float range)
	{
		// 120
		float minDirection = direction - (range / 2); // 80
		float maxDirection = direction + (range / 2); // 160
		float currentDirection = Input.compass.trueHeading;

		bool result = false;
		if (minDirection < 0)
		{
			float tempMinDirection = 360 + minDirection;
			if (tempMinDirection < currentDirection && currentDirection < 360) result = true;
			else if (0 <= currentDirection && currentDirection < maxDirection) result = true;
			else result = false;
		}
		else if (360 < maxDirection)
		{
			float tempMaxDirection = maxDirection - 360;
			if (0 <= currentDirection && currentDirection < tempMaxDirection) result = true;
			else if (minDirection < currentDirection && currentDirection < 360) result = true;
			else result = false;
		}
		else
		{
			if (minDirection < currentDirection && currentDirection < maxDirection) result = true;
			else result = false;
		}
		return result;
	}





	private void OnSettingFileUpdated()
	{

	}

	private void OnBluetoothStateChanged(BluetoothLowEnergyState state)
	{
		switch (state)
		{
			case BluetoothLowEnergyState.TURNING_OFF:
				break;
			case BluetoothLowEnergyState.TURNING_ON:
				break;
			case BluetoothLowEnergyState.UNKNOWN:
				break;
			case BluetoothLowEnergyState.RESETTING:
				break;
			case BluetoothLowEnergyState.UNAUTHORIZED:
				break;
			case BluetoothLowEnergyState.UNSUPPORTED:
				break;
			case BluetoothLowEnergyState.POWERED_OFF:
				break;
			case BluetoothLowEnergyState.POWERED_ON:
				break;
			default:
				break;
		}
	}

	private void OnBeaconRangeChanged(Beacon[] beacons)
	{
		Debug.Log("----- Beacon Changed");
		foreach (Beacon b in beacons)
		{
			var index = mybeacons.IndexOf(b);
			if (index == -1)
			{
				mybeacons.Add(b);
			}
			else
			{
				mybeacons[index] = b;
			}
		}
		for (int i = mybeacons.Count - 1; i >= 0; --i)
		{
			if (mybeacons[i].lastSeen.AddSeconds(10) < DateTime.Now)
			{
				mybeacons.RemoveAt(i);
			}
		}
		//_receivedCountList = new List<int>() { 0, 0, 0 };
		//_rssiList = new List<int>() { -200, -200, -200 };
		//_rssiList1 = new List<int>();
		//_rssiList2 = new List<int>();
		//_rssiList3 = new List<int>();

		DisplayOnBeaconFound();
	}





	public void StartScan()
	{
		if (_broadcastState == BroadcastState.DEACTIVATE)
		{
			Debug.Log("Start Scan");
			_broadcastState = BroadcastState.ACTIVATE;

			iBeaconReceiver.BeaconRangeChangedEvent += OnBeaconRangeChanged;
			iBeaconReceiver.regions = new iBeaconRegion[] { new iBeaconRegion(_regionName, new Beacon()) };
			//iBeaconReceiver.regions = new iBeaconRegion[] { new iBeaconRegion(_regionName, new Beacon(s_UUID, "")) };
			iBeaconReceiver.Scan();

			if (ScanStarted != null) ScanStarted();
		}
	}
	
	public void StopScan()
	{
		if (_broadcastState == BroadcastState.ACTIVATE)
		{
			Debug.Log("Stop Scan");
			_broadcastState = BroadcastState.DEACTIVATE;

			iBeaconReceiver.BeaconRangeChangedEvent -= OnBeaconRangeChanged;
			iBeaconReceiver.Stop();

			_rawBeaconLog.text = "";
			_filteredBeaconLog.text = "";
			_selectedBeaconLog.text = "";

			if (ScanStoped != null) ScanStoped();
		}
	}
}