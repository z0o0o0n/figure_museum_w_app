﻿namespace com.mod.fm.beacon
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Beacon : MonoBehaviour
	{
		[SerializeField]
		private Setting _setting;
		private List<BeaconData> _beaconDatas;

		private void Start()
		{
			//CreateBeaconData();
		}

		private void Update()
		{

		}





		//public void CreateBeaconData()
		//{
		//	_beaconDatas = new List<BeaconData>();
		//	for(int i = 0; i < 10; i++)
		//	{
		//		BeaconData beaconData = new BeaconData();
		//		beaconData.id = i.ToString();
		//		beaconData.namespaceId = _setting.GetSettingData().beaconNamespaceIdList[i];
		//		beaconData.zoneIds = _setting.GetSettingData().beaconZoneIdList[i];
		//		beaconData.rssiList = new List<int>();
		//		beaconData.rssi = -200;
		//		beaconData.normalRssi = -200;
		//	}
		//}

		public void AddBeaconInfo(string namespaceId, int rssi)
		{
			GetBeaconDataByNamespaceId(namespaceId).rssiList.Add(rssi);
		}

		public BeaconData GetBeaconDataByNamespaceId(string namespaceId)
		{
			BeaconData result = null;
			for(int i = 0; i < _beaconDatas.Count; i++)
			{
				if (_beaconDatas[i].namespaceId == namespaceId) result = _beaconDatas[i];
			}
			return result;
		}
	}
}