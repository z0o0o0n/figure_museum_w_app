﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanModeButton : MonoBehaviour
{
	[SerializeField]
	private ModeButton _modeButton;
	[SerializeField]
	private Text _scanButtonText;
	[SerializeField]
	private Text _broadcastStateText;
	[SerializeField]
	private BeaconService _beaconService;

	private void Awake()
	{
		_modeButton.ModeChanged += OnModeChanged;

		_beaconService.ScanStarted += OnBeaconScanStarted;
		_beaconService.ScanStoped += OnBeaconScanStoped;

		Debug.Log("current mode: " + _modeButton.GetCurrentMode());
		UpdateScanButtonText(_modeButton.GetCurrentMode());
	}

	private void Start ()
	{
		
	}

	private void OnEnable()
	{
		_modeButton.SetCurrentMode(_beaconService.isActivated);
	}

	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_modeButton.ModeChanged -= OnModeChanged;

		_beaconService.ScanStarted -= OnBeaconScanStarted;
		_beaconService.ScanStoped -= OnBeaconScanStoped;
	}





	private void OnBeaconScanStarted()
	{
		UpdateScanButtonText(true);
	}

	private void OnBeaconScanStoped()
	{
		UpdateScanButtonText(false);
	}

	private void UpdateScanButtonText(bool mode)
	{
		if (mode)
		{
			_scanButtonText.text = "Stop Scan";
			_broadcastStateText.text = "Scanning...";
			_broadcastStateText.color = Color.green;
		}
		else if (!mode)
		{
			_scanButtonText.text = "Start Scan";
			_broadcastStateText.text = "Deactivation";
			_broadcastStateText.color = Color.red;
		}
	}





	private void OnModeChanged(bool mode)
	{
		if (mode)
		{
			Debug.Log("Start Scan");
			_beaconService.StartScan();
		}
		else if (!mode)
		{
			Debug.Log("Stop Scan");
			_beaconService.StopScan();
		}
	}
}
