﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RSSIUtil 
{
    public static string GetStringFromList(List<int> list)
    {
        string value = "";
        for (int i = 0; i < list.Count; i++)
        {
            value += list[i].ToString() + ",";
        }
        return value;
    }

    public static int GetNormalRSSI(List<int> list)
    {
        int sum = 0;
        for (int i = 0; i < list.Count; i++)
        {
            sum += list[i];
        }

        if (list.Count == 0) return -200;
        else return sum / (list.Count);
    }

    public static float GetRSSIQuality(int rssi)
    {
        //-100 ~ -80 : 0 ~ 70
        float rate = 70;
        float value = 0;
        if (rssi <= -100) value = 0;
        else if (rssi >= -80) value = rate;
        else
        {
            value = (rssi + 100) * (rate / 20);
        }

        return value;
    }

    public static float GetRSSICountQuality(int rssiCount)
    {
        // 0 ~ 10 : 0 ~ 30
        float rate = 30;
        float value = 0;
        if (rssiCount <= 0) value = 0;
        else if (rssiCount >= 10) value = rate;
        else
        {
            value = rssiCount * (rate / 10);
        }

        return value;
    }
}
