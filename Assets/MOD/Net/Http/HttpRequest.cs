﻿namespace MOD.Net.Http
{
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;
	using DG.Tweening;
	using UnityEngine.Networking;

	/*
	 * ver: 0.0.4
	 * history:
	 * 0.0.4 www.error 우뮤를 null과 ""공백 문자열 모두를 비교하는 방식으로 수정함. ex) string.IsNullOrEmpty(www.error)
	 * 0.0.3 www.error 유무를 null로 비교할 경우 error가 없는데 error를 발생시킴. "" 공백 문자열과 비교하는 방식으로 수정함.
	 * 0.0.2 PostWithForm() 사진 바이너리 등 원하는 Form을 구성해 Post로 전송할 수 있는 기능 추가
	 * 0.0.1 GET, POST 방식으로 간단한 string 변수를 전송할 수 있는 기능 개발
	 */
	public class HttpRequest : MonoBehaviour
	{
		public delegate void HttpRequestDelegate(string id, string wwwText);
		public event HttpRequestDelegate Complete;
		public event HttpRequestDelegate Error;
		public event HttpRequestDelegate TimeOut;

		private Coroutine _currentCoroutine;
		private bool _isDebug = false;




		public void Get(string id, string url, float limitTime = 15f)
		{
			WWW www = new WWW(url);

			DOTween.Kill("WaitForRequest." + GetInstanceID());
			DOVirtual.DelayedCall(limitTime, () => OnTimeOut(id, www)).SetId("WaitForRequest." + GetInstanceID());

			if (_currentCoroutine != null) StopCoroutine(_currentCoroutine);
			_currentCoroutine = StartCoroutine(WaitForRequest(id, www));
		}

		public void Post(string id, string url, IDictionary<string, string> stringArgs, float limitTime = 15f)
		{
			WWWForm form = new WWWForm();
			foreach (KeyValuePair<string, string> stringArg in stringArgs)
			{
				form.AddField(stringArg.Key, stringArg.Value);
			}

			WWW www = new WWW(url, form.data);

			DOTween.Kill("WaitForRequest." + GetInstanceID());
			DOVirtual.DelayedCall(limitTime, () => OnTimeOut(id, www)).SetId("WaitForRequest." + GetInstanceID());

			if (_currentCoroutine != null) StopCoroutine(_currentCoroutine);
			_currentCoroutine = StartCoroutine(WaitForRequest(id, www));
		}

		// 단순 string 변수 말고 다양한 타입의 데이터를 전송할 때 사용 (예를들면 사진 바이너리)
		public void PostWithForm(string id, string url, WWWForm wwwForm, float limitTime = 15f)
		{
			WWW www = new WWW(url, wwwForm);

			DOTween.Kill("WaitForRequest." + GetInstanceID());
			DOVirtual.DelayedCall(limitTime, () => OnTimeOut(id, www)).SetId("WaitForRequest." + GetInstanceID());

			if (_currentCoroutine != null) StopCoroutine(_currentCoroutine);
			_currentCoroutine = StartCoroutine(WaitForRequest(id, www));
		}

		private IEnumerator WaitForRequest(string id, WWW www)
		{
			do
			{
				yield return null;
			}
			while (!www.isDone);

			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.Log("Http Request error / " + www.error);
				DOTween.Kill("WaitForRequest." + GetInstanceID());
				if (Error != null) Error(id, www.text);
				www.Dispose();
				yield break;
			}

			Debug.Log("Http Request done / " + www.text);
			DOTween.Kill("WaitForRequest." + GetInstanceID());
			if (Complete != null) Complete(id, www.text);
			www.Dispose();
		}

		private void OnTimeOut(string id, WWW www)
		{
			Debug.Log("Http Request / Time Out");
			StopCoroutine(_currentCoroutine);
			_currentCoroutine = null;
			if (TimeOut != null) TimeOut(id, "Timeout");
			www.Dispose();
		}

		private IEnumerator WaitForMultipartRequest(string id, UnityWebRequest www)
		{
			do
			{
				yield return null;
			}
			while (!www.isDone);

			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.Log("Http Request error / " + www.responseCode + " / " + www.error);
				DOTween.Kill("WaitForRequest." + GetInstanceID());
				if (Error != null) Error(id, www.responseCode.ToString());
				www.Dispose();
				yield break;
			}

			Debug.Log("Http Request done / " + www.responseCode.ToString());
			DOTween.Kill("WaitForRequest." + GetInstanceID());
			if (Complete != null) Complete(id, www.responseCode.ToString());
			www.Dispose();
		}

		private void OnMulipartTimeOut(string id, UnityWebRequest www)
		{
			Debug.Log("Http Multipart Request / Time Out");
			StopCoroutine(_currentCoroutine);
			_currentCoroutine = null;
			if (TimeOut != null) TimeOut(id, "Timeout");
			www.Dispose();
		}
	}
}